/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic;

import com.github.javafaker.Faker;
import data.Emprendedor;
import data.Inversionista;
import data.Proyecto;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author angel
 */
class ManejoArchivos {

    public static void guardarEmprendedores(File archivo, HashTable<Emprendedor> emprendedoresMap) {

        try {
            FileOutputStream output = new FileOutputStream(archivo);
            ObjectOutputStream writer = new ObjectOutputStream(output);

            // Write objects to file
            writer.writeObject(emprendedoresMap);

            writer.close();
            output.close();

        } catch (FileNotFoundException e) {

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static HashTable<Emprendedor> leerEmprendedores(File archivo) {

        HashTable<Emprendedor> emprendedoresMap = new HashTable<>(10);
        try {

            FileInputStream input = new FileInputStream(archivo);
            ObjectInputStream reader = new ObjectInputStream(input);

            // Read objects
            emprendedoresMap = (HashTable<Emprendedor>) reader.readObject();

            reader.close();
            input.close();

        } catch (FileNotFoundException e) {
            
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return emprendedoresMap;
    }

    public static void guardarInversionistas(File archivo, HashTable<Inversionista> inversionistasMap) {

        try {
            FileOutputStream output = new FileOutputStream(archivo);
            ObjectOutputStream writer = new ObjectOutputStream(output);

            // Write objects to file
            writer.writeObject(inversionistasMap);

            writer.close();
            output.close();

        } catch (FileNotFoundException e) {

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static HashTable<Inversionista> leerInversionistas(File archivo) {

        HashTable<Inversionista> inversionistasMap = new HashTable<>(10);
        try {

            FileInputStream input = new FileInputStream(archivo);
            ObjectInputStream reader = new ObjectInputStream(input);

            // Read objects
            inversionistasMap = (HashTable<Inversionista>) reader.readObject();

            reader.close();
            input.close();

        } catch (FileNotFoundException e) {
            
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return inversionistasMap;
    }

    public static void guardarProyectos(File archivo, HashTable<Proyecto> proyectosMap) {

        try {
            FileOutputStream output = new FileOutputStream(archivo);
            ObjectOutputStream writer = new ObjectOutputStream(output);

            // Write objects to file
            writer.writeObject(proyectosMap);

            writer.close();
            output.close();

        } catch (FileNotFoundException e) {

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static HashTable<Proyecto> leerProyectos(File archivo) {

        HashTable<Proyecto> proyectosMap = new HashTable<>(10);
        try {

            FileInputStream input = new FileInputStream(archivo);
            ObjectInputStream reader = new ObjectInputStream(input);

            // Read objects
            proyectosMap = (HashTable<Proyecto>) reader.readObject();

            reader.close();
            input.close();

        } catch (FileNotFoundException e) {

        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return proyectosMap;
    }
    
    public static void guardarIdProyecto(File archivo, int id) {

        try {
            FileOutputStream output = new FileOutputStream(archivo);
            ObjectOutputStream writer = new ObjectOutputStream(output);

            // Write objects to file
            writer.writeObject(id);

            writer.close();
            output.close();

        } catch (FileNotFoundException e) {

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static int leerIdProyecto(File archivo) {
        int id = 0;
        try {

            FileInputStream input = new FileInputStream(archivo);
            ObjectInputStream reader = new ObjectInputStream(input);

            // Read objects
            id = (int) reader.readObject();

            reader.close();
            input.close();

        } catch (FileNotFoundException e) {

        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return id;
    }
    
    public static void leerCSV10K(String archivo) {
        BufferedReader br = null;

        try {

            br = new BufferedReader(new FileReader(archivo));// Cambiar archivo
            String line = br.readLine();
            int Id_Auto = 0;
            line = br.readLine();
            while (Id_Auto < 10000) {
                String[] fields = line.split(",");
                //System.out.println(fields[1]);
                Proyecto p = new Proyecto(Id_Auto, fields[0], 
                        fields[1], fields[2], Long.parseLong(fields[3]), Main.elegirUsuario(), fields[4],Main.asignarlikes());
                Main.agregarProyecto(p);
                        
                line = br.readLine();
                Id_Auto = Id_Auto + 1;
            }

        } catch (Exception e) {
       
        } finally {
            if (null != br) {
                try {
                    br.close();
                } catch (IOException ex) {
                    
                }
            }
        }
    }
    public static void leerCSV100K(String archivo) {
        BufferedReader br = null;

        try {

            br = new BufferedReader(new FileReader(archivo));// Cambiar archivo
            String line = br.readLine();
            int Id_Auto = 0;
            line = br.readLine();
            while (Id_Auto < 100000) {
                String[] fields = line.split(",");
                //System.out.println(fields[1]);
                Proyecto p = new Proyecto(Id_Auto, fields[0], 
                        fields[1], fields[2], Long.parseLong(fields[3]), Main.elegirUsuario(), fields[4],Main.asignarlikes());
                Main.agregarProyecto(p);
                        
                line = br.readLine();
                Id_Auto = Id_Auto + 1;
            }

        } catch (Exception e) {

        } finally {
            if (null != br) {
                try {
                    br.close();
                } catch (IOException ex) {
                    
                }
            }
        }
    }
    public static void leerCSV500K(String archivo) {
        BufferedReader br = null;

        try {

            br = new BufferedReader(new FileReader(archivo));// Cambiar archivo
            String line = br.readLine();
            int Id_Auto = 0;
            line = br.readLine();
            while (Id_Auto < 500000) {
                String[] fields = line.split(",");
                //System.out.println(fields[1]);
                Proyecto p = new Proyecto(Id_Auto, fields[0], 
                        fields[1], fields[2], Long.parseLong(fields[3]), Main.elegirUsuario(), fields[4],Main.asignarlikes());
                Main.agregarProyecto(p);
                        
                line = br.readLine();
                Id_Auto = Id_Auto + 1;
            }

        } catch (Exception e) {

        } finally {
            if (null != br) {
                try {
                    br.close();
                } catch (IOException ex) {
                    
                }
            }
        }
    }  
    public static void leerCSV1M(String archivo) {
        BufferedReader br = null;

        try {

            br = new BufferedReader(new FileReader(archivo));// Cambiar archivo
            String line = br.readLine();
            int Id_Auto = 0;
            line = br.readLine();
            while (Id_Auto < 1000000) {
                String[] fields = line.split(",");
                //System.out.println(fields[1]);
                Proyecto p = new Proyecto(Id_Auto, fields[0], 
                        fields[1], fields[2], Long.parseLong(fields[3]), Main.elegirUsuario(), fields[4],Main.asignarlikes());
                Main.agregarProyecto(p);
                        
                line = br.readLine();
                Id_Auto = Id_Auto + 1;
            }

        } catch (Exception e) {

        } finally {
            if (null != br) {
                try {
                    br.close();
                } catch (IOException ex) {
                    
                }
            }
        }
    }
    public static void leerCSV10M(String archivo) {
        BufferedReader br = null;

        try {

            br = new BufferedReader(new FileReader(archivo));// Cambiar archivo
            String line = br.readLine();
            int Id_Auto = 0;
            line = br.readLine();
            while (null != line) {
                String[] fields = line.split(",");
                //System.out.println(fields[1]);
                for (int i = 0; i < 10; i++) {
                    Proyecto p = new Proyecto(Id_Auto, fields[0],
                            fields[1], fields[2], Long.parseLong(fields[3]), Main.elegirUsuario(), fields[4],Main.asignarlikes());
                    Main.agregarProyecto(p);
                    Id_Auto = Id_Auto + 1;
                }
                line = br.readLine();
            }

        } catch (Exception e) {

        } finally {
            if (null != br) {
                try {
                    br.close();
                } catch (IOException ex) {

                }
            }
        }
    }
    public static void leerEmprendedorCSV(String archivo) {
        BufferedReader br = null;

        try {
            Faker faker = new Faker();
            br = new BufferedReader(new FileReader(archivo));// Cambiar archivo
            String line = br.readLine();
            line = br.readLine();
            while (null != line) {
                String[] fields = line.split(",");
                Emprendedor p = new Emprendedor(fields[0], 
                        fields[1], fields[2],fields[3] , (int)faker.number().randomNumber(), fields[4]);
                Main.getEmprendedores().put(p, fields[1]);
                        
                line = br.readLine();
            }

        } catch (Exception e) {

        } finally {
            if (null != br) {
                try {
                    br.close();
                } catch (IOException ex) {
                    
                }
            }
        }
    }
    
    public static void leerCSV10KNuevo(String archivo) {
        System.out.println("Prueba 10 k");
        BufferedReader br = null;

        try {

            br = new BufferedReader(new FileReader(archivo));// Cambiar archivo
            String line = br.readLine();
            int Id_Auto = 0;
            line = br.readLine();
            int cont = 1;
            while (Id_Auto < 10000) {
                String[] fields = line.split(",");
                //System.out.println(fields[1]);
                Proyecto p = new Proyecto(Integer.parseInt(fields[0]), 
                        fields[1], fields[2],fields[3], Long.parseLong(fields[4]), Main.elegirUsuario(), fields[5], cont);
                Main.agregarProyecto(p);
                        
                line = br.readLine();
                Id_Auto = Id_Auto + 1;
            }

        } catch (Exception e) {

        } finally {
            if (null != br) {
                try {
                    br.close();
                } catch (IOException ex) {
                    
                }
            }
        }
    }

}
