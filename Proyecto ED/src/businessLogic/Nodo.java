
package businessLogic;

import java.io.Serializable;


public class Nodo<T> implements Serializable{
    private T  key;
    private Nodo<T> next;
    private Nodo<T> prev;

    public Nodo(T key) {
        this.key = key;
        this.next = null;
    }

    public T getKey() {
        return key;
    }

    public void setKey(T key) {
        this.key = key;
    }

    public Nodo<T> getNext() {
        return next;
    }

    public void setNext(Nodo<T> next) {
        this.next = next;
    }

    public Nodo<T> getPrev() {
        return prev;
    }

    public void setPrev(Nodo<T> prev) {
        this.prev = prev;
    }
    
    
    public Nodo(T value, Nodo<T> nodoPrev, Nodo<T> nodoNext){
        this.key = value;
        this.prev = nodoPrev;
        this.next = nodoNext;
    }
    
    public Nodo(T value, Nodo<T> nodoNext){
        this.key = value;
        this.prev = null;
        this.next = nodoNext;
    }

    @Override
    public String toString() {
        return String.valueOf(key);
    }
}
