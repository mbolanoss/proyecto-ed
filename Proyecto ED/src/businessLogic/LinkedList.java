package businessLogic;

import java.io.Serializable;

public class LinkedList<T> implements Serializable {

    private Nodo<T> head;
    private Nodo<T> tail;
    private int size;

    public LinkedList() {
        this.size = 0;
        this.head = new Nodo<T>(null);
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void pushFront(T key) {
        Nodo<T> nuevo = new Nodo<>(key);
        if (this.size == 0) {
            this.head.setNext(nuevo);
        } else {
            nuevo.setNext(this.head.getNext());
            this.head.setNext(nuevo);
        }
        this.size++;
    }

    public void pushBack(T key) {
        Nodo<T> nuevo = new Nodo<>(key);
        if (this.size == 0) {
            this.head = nuevo;
            this.tail = nuevo;
        } else {
            this.tail.setNext(nuevo);
            this.tail = nuevo;
        }
        this.size++;
    }

    public int size() {
        return this.size;
    }

    public boolean contains(T key) {
        int tempSize = this.size;
        Nodo<T> temp = this.head.getNext();
        while (tempSize != 0) {
            if (temp.getKey().equals(key)) {
                return true;
            }
            temp = temp.getNext();
            tempSize--;
        }
        return false;
    }

    public boolean remove(T value) {

        if (size == 0) {
            return false;
        }
        boolean found = false;
        Nodo current = head;
        int tempSize = size;
        if (head != null) {
            while (!current.getNext().getKey().equals(value)) {
                current = current.getNext();
                tempSize--;
                if(tempSize == 0) return false;
            }
            
            current.setNext(current.getNext().getNext());
            size--;
            return true;

        }
        return false;
    }

    @Override
    public String toString() {
        int tempSize = this.size;
        String list = "";
        Nodo<T> temp = this.head.getNext();
        while (tempSize != 0) {
            list += temp.getKey().toString() + "\n-------------------------\n";
            temp = temp.getNext();
            tempSize--;
        }
        return list;
    }

    public Nodo<T> getHead() {
        return head;
    }

    public void setHead(Nodo<T> head) {
        this.head = head;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

}
