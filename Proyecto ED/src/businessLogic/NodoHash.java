
package businessLogic;

import java.io.Serializable;

public class NodoHash<T> implements Serializable{

    private T valor;
    private String claveString;
    private int claveInt;

    public NodoHash(T valor, String clave, int capacidad) {
        this.valor = valor;
        this.claveString = clave;
        this.claveInt = HashTable.stringHash(clave, capacidad);
    }

    public NodoHash(T valor, int clave) {
        this.valor = valor;
        this.claveInt = clave;
        this.claveString = "";
    }

    public T getValor() {
        return valor;
    }

    public void setValor(T valor) {
        this.valor = valor;
    }

    public String getClaveString() {
        return claveString;
    }

    public void setClaveString(String claveString) {
        this.claveString = claveString;
    }

    public int getClaveInt() {
        return claveInt;
    }

    public void setClaveInt(int claveInt) {
        this.claveInt = claveInt;
    }

    public boolean esVacio() {
        return claveInt == -1 && claveString.equals("");
    }

}
