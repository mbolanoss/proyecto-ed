
package businessLogic;

import java.util.Arrays;


public class DynamicArray<T> {
    private T[] array;
    private int maxSize;
    private int size;

    public DynamicArray(int maxSize, Class<T> type) {
        this.maxSize = maxSize;
        this.size = 0;
        this.array = (T[]) java.lang.reflect.Array.newInstance(type, maxSize);
    }
    
    public void insert(T key, int pos){
        checkSize();
        this.array[pos] = key;
        size++;
    }
    
    public void insert(T key){
        checkSize();
        this.array[size] = key;
        size++;
    }
    
    public T get(int pos){
        return this.array[pos]; 
    }
    
    public boolean delete(T key){
        for(int i = 0; i < size; i++){
            if(this.array[i].equals(key)){
                this.array[i] = null;
                shiftLeft(i);
                size--;
                return true;
            }
        }
        return false;
    }
    
    private void shiftLeft(int pos){
        while(pos != size - 1){
            this.array[pos] = this.array[pos + 1];
            this.array[pos + 1] = null;
            pos++;
        }
    }
    
    public boolean isEmpty(){
        return size == 0;
    }
    
    private void checkSize(){
        if(size == maxSize){
            this.maxSize *= 2;
            this.array = Arrays.copyOf(array, maxSize);
        }
    }
    
    public int getSize(){
        return this.size;
    }
    
    public T[] getArray(){
        return this.array;
    }
    
    
}
