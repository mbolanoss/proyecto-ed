package businessLogic;

import data.Emprendedor;
import data.Inversionista;
import data.Proyecto;
import gui.LogIn;
import gui.Roles;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.*;//lo puse yo 
import java.util.Random;

public class Main {

    public static HashTable<Proyecto> proyectos;
    public static HashTable<Emprendedor> emprendedores;
    public static HashTable<Inversionista> inversionistas;
    private static File archivoEmprendedores = new File("emprendedores.txt");
    private static File archivoProyectos = new File("proyectos.txt");
    private static File archivoInversionistas = new File("inversionistas.txt");
    private static File archivoIdProyecto = new File("idProyecto.txt");
    private static int idProyecto;
    public static boolean istopgeneral;
    public static String categoria;

    private static String usuarioLogeado;

    public static LogIn login;

    public static void main(String[] args) {
        inicializarDatos();
        usuarioLogeado = "";
        login = new LogIn();
        
        
    }

    public static LogIn getLogin() {
        return login;
    }

    public static String getUsuarioLogeado() {
        return usuarioLogeado;
    }

    public static void setUsuarioLogeado(String usuario) {
        usuarioLogeado = usuario;
    }

    public static HashTable<Proyecto> getProyectos() {
        return proyectos;
    }

    public static HashTable<Emprendedor> getEmprendedores() {
        return emprendedores;
    }

    public static HashTable<Inversionista> getInversionistas() {
        return inversionistas;
    }

    public static void setEmprendedores(HashTable<Emprendedor> emprendedores) {
        Main.emprendedores = emprendedores;
    }

    public static void setInversionistas(HashTable<Inversionista> inversionistas) {
        Main.inversionistas = inversionistas;
    }

    public static int getIdProyecto() {
        return idProyecto;
    }

    public static void setIdProyecto(int idProyecto) {
        Main.idProyecto = idProyecto;
    }
    
    public static void aumentarInversion(int id, int cantidad){
        proyectos.get(id).aumentarInversion(cantidad);
    }

    private static void inicializarDatos() {
        proyectos = new HashTable<>(10);
        emprendedores = new HashTable<>(10);
        inversionistas = new HashTable<>(10);
        idProyecto = 0;
        
        if (archivoEmprendedores.exists()) {
            emprendedores = ManejoArchivos.leerEmprendedores(archivoEmprendedores);
        }
        if (archivoInversionistas.exists()) {
            inversionistas = ManejoArchivos.leerInversionistas(archivoInversionistas);
        }
        if (archivoProyectos.exists()) {
            proyectos = ManejoArchivos.leerProyectos(archivoProyectos);
        }
        if (archivoIdProyecto.exists()) {
            idProyecto = ManejoArchivos.leerIdProyecto(archivoIdProyecto);
        }
        
    }

    public static void guardarDatos() {
        ManejoArchivos.guardarProyectos(archivoProyectos, proyectos);
        ManejoArchivos.guardarEmprendedores(archivoEmprendedores, emprendedores);
        ManejoArchivos.guardarInversionistas(archivoInversionistas, inversionistas);
        ManejoArchivos.guardarIdProyecto(archivoIdProyecto, idProyecto);
    }

    public static HashMap<Integer, Proyecto> buscarProyectos(String usuario) {
        HashMap<Integer, Proyecto> proyectosEmp = new HashMap<>();
        for (Proyecto p : proyectos.values(Proyecto.class)) {
            if (p.getIdCreador().equals(usuario)) {
                proyectosEmp.put(p.getId(), p);
            }
        }
        return null;
    }

    public static String elegirUsuario() {
        int cont = 0;
        String[] usuarios = new String[emprendedores.getCantidadElementos()];
        for (Emprendedor e : emprendedores.values(Emprendedor.class)) {
            usuarios[cont] = e.getUsuario();
            cont++;
        }
        Random random = new Random();
        int num = 0;
        num = random.nextInt(emprendedores.getCantidadElementos());
        return usuarios[num];
    }
    
    public static int asignarlikes() {
        Random random = new Random();
        int num = 0;
        num = random.nextInt(100);
        return num;
    }

    public static void agregarProyecto(Proyecto p) {
        proyectos.put(p, p.getId());
    }

    public static void agregarInversionista(Inversionista i) {
        inversionistas.put(i, i.getUsuario());
    }

    public static void agregarEmprendedor(Emprendedor e) {
        emprendedores.put(e, e.getUsuario());
    }

    public static void aumentarLikes(int id) {
        proyectos.get(id).setLikes(proyectos.get(id).getLikes() + 1);
    }

    public static void quitarLike(int id) {
        proyectos.get(id).setLikes(proyectos.get(id).getLikes() - 1);
    }

    public static void actualizarProyecto(Proyecto nuevo) {
        proyectos.remove(nuevo.getId());
        proyectos.put(nuevo, nuevo.getId());
    }
    
    public static void actualizarIdCreador (String nuevoUsuario, String viejoUsuario){
        for(int i = 0; i < proyectos.getCantidadElementos(); i++){
            if(proyectos.values(Proyecto.class)[i].getIdCreador().equals(viejoUsuario)){
                proyectos.values(Proyecto.class)[i].setIdCreador(nuevoUsuario);
            }
        }
    }
}
