package businessLogic;

import data.Emprendedor;
import data.Inversionista;
import data.Proyecto;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class Validacion {

    public static boolean validarUsuarioRegistro(String usuario) {
        //Scanner scan = new Scanner(System.in);

        if (Main.getEmprendedores().containsKey(usuario) || Main.getInversionistas().containsKey(usuario)) {
            JOptionPane.showMessageDialog(null, "Usuario ya registrado, ingrese otro usuario");
            return false;
        }

        return true;
    }

    public static boolean validarCedula(int cedula) {

        while (cedulaRepetida(cedula)) {

            JOptionPane.showMessageDialog(null, "Cédula ya registrada, ingrese de nuevo su cédula");

        }
        return true;
    }

    public static boolean validarEmail(String email) {
        char[] caracteresNoPermitidos = {'(', ')', '<', '>', ',', ';', ':', '"', '[', ']', 'ç', '%', '&'};
        boolean emailValido = true;

        while (true) {
            if (email.indexOf("@") < 1 || email.length() > 320 || email.substring(0,
                    email.indexOf("@")).length() > 64) {
                emailValido = false;
            }
            for (char i : caracteresNoPermitidos) {
                if (email.indexOf(i) != -1) {
                    emailValido = false;
                    JOptionPane.showMessageDialog(null, "Estas usando caracteres invalidos");
                }
            }
            if (emailValido == false) {
                //System.out.println("Email invalido, ingrese de nuevo su email");
                JOptionPane.showMessageDialog(null, "Email invalido, ingrese de nuevo su email");
                emailValido = true;
            } else {
                break;
            }
        }
        return true;
    }

    private static boolean cedulaRepetida(int cedula) {
        ArrayList<Integer> cedulas = new ArrayList<>();
        if (Main.emprendedores.getCantidadElementos() != 0) {
            for (Emprendedor e : Main.getEmprendedores().values(Emprendedor.class)) {
                cedulas.add(e.getCedula());
            }
        }

        if (Main.inversionistas.getCantidadElementos() != 0) {
            for (Inversionista i : Main.getInversionistas().values(Inversionista.class)) {
                cedulas.add(i.getCedula());
            }
        }

        for (int i : cedulas) {
            if (cedula == i) {
                return true;
            }
        }
        return false;
    }

    public static boolean validarUsuarioLogin(String usuario) {
        Scanner scan = new Scanner(System.in);
        while (true) {
            if (Main.getEmprendedores().containsKey(usuario) || Main.getInversionistas().containsKey(usuario)) {
                return true;
                //break;
            } else {
                System.out.println("Usuario no registrado");
//                int opcion = GUI.imprimirOpcionesSalidaRegreso();
//                switch (opcion) {
//                    case 1:
//                        Main.mostrarMenuPrincipal();
//                        break;
//                    case 2:
//                        GUI.imprimirInicioSesion();
//                        break;
//                }
                return false;
            }
        }
    }

    public static boolean validarContrasenaLogin(String contrasena, String usuario) {
        boolean respuesta = false;
        if (!(Main.getEmprendedores().isEmpty())) {
            if (Main.getEmprendedores().containsKey(usuario)) {
                if (Main.getEmprendedores().get(usuario).getContrasena().equals(contrasena)) {
                    //GUI.imprimirMenuUsuario(usuario);
                    respuesta = true;
                } else {
                    System.out.println("Contraseña incorrecta, ingrese de nuevo su contraseña");
                }
            }
        }
        if (!(Main.getInversionistas().isEmpty())) {
            if (Main.getInversionistas().containsKey(usuario)) {
                if (Main.getInversionistas().get(usuario).getContrasena().equals(contrasena)) {
                    //GUI.imprimirMenuUsuario(usuario);
                    respuesta = true;
                } else {
                    System.out.println("Contraseña incorrecta, ingrese de nuevo su contraseña");
                }
            }
        }
        return respuesta;
//        Scanner scan = new Scanner(System.in);
//        while (true) {
//            if (!(Main.getEmprendedores().isEmpty())) {
//                if (Main.getEmprendedores().containsKey(usuario)) {
//                    if (Main.getEmprendedores().get(usuario).getContrasena().equals(contrasena)) {
//                        GUI.imprimirMenuUsuario(usuario);
//                        break;
//                    } else {
//                        System.out.println("Contraseña incorrecta, ingrese de nuevo su contraseña");
//                        contrasena = scan.nextLine();
//                    }
//                }
//            }
//            if (!(Main.getInversionistas().isEmpty())) {
//                if (Main.getInversionistas().containsKey(usuario)) {
//                    if (Main.getInversionistas().get(usuario).getContrasena().equals(contrasena)) {
//                        GUI.imprimirMenuUsuario(usuario);
//                        break;
//                    } else {
//                        System.out.println("Contraseña incorrecta, ingrese de nuevo su contraseña");
//                        contrasena = scan.nextLine();
//                    }
//                }
//            }
//        }
    }

    public static int validarFormatoCedula() {
        Scanner scan = new Scanner(System.in);
        int cedula = 0;
        while (true) {
            try {
                cedula = scan.nextInt();

                break;
            } catch (InputMismatchException e) {
                System.out.println("La cédula solo puede contener numeros.\ningrese de nuevo su cédula:");
                scan.next();

            }
        }
        return cedula;
    }

    public static int validarIdProyecto(String usuario) {
        boolean idValido = false;
        int id = 0;
        Scanner scan = new Scanner(System.in);
        while (true) { //Se revisa si ingreso solo numeros
            try {
                id = scan.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Solo se pueden ingresar números. Ingrese el id de nuevo:");
                scan.next();
                continue;
            }

            for (Proyecto p : Main.getProyectos().values(Proyecto.class)) {
                if (p.getIdCreador().equals(usuario)) {
                    if (p.getId() == id) {
                        idValido = true;
                        break;
                    }
                }
            }
            if (!idValido) {
                System.out.println("Id de proyecto no encontrado. Ingreselo de nuevo:");
                scan.next();
            } else {
                break;
            }
        }
        return id;
    }

    public static boolean presupCorrecto(String presupuesto) {
        try {
            Integer.parseInt(presupuesto);
            return false;
        } catch (NumberFormatException ex) {
            return true;
        }
    }

    public static boolean validarInt(String valor) {
        try {
            Integer.parseInt(valor);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }
}
