
package businessLogic;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.Arrays;

public class HashTable<T> implements Serializable {

    private int capacidad;
    private NodoHash<T>[] arr;
    private int cantidadElementos;

    public HashTable(int capacidad) {
        this.capacidad = siguientePrimo(capacidad);
        this.arr = new NodoHash[this.capacidad];
        cantidadElementos = 0;
    }

    private void reHash() {
        NodoHash[] arrTmp = Arrays.copyOf(arr, capacidad);

        this.capacidad = siguientePrimo(capacidad * 2);
        this.arr = new NodoHash[capacidad];

        for (int i = 0; i < arrTmp.length; i++) {
            if (arrTmp[i] != null) {

                NodoHash nodo = arrTmp[i];
                int hash = 0;
                if (!nodo.getClaveString().equals("")) {
                    hash = stringHash(nodo.getClaveString(), capacidad);
                } else {
                    hash = nodo.getClaveInt();
                }

                int colisiones = 0;
                int posicion = hashFunc1(hash, colisiones);
                while (arr[posicion] != null) {
                    colisiones++;
                    posicion = hashFunc1(hash, colisiones);
                }

                arr[posicion] = nodo;
            }

        }

    }

    public void remove(int clave) {
        int colisiones = 0;
        int posicion = hashFunc1(clave, colisiones);
        while (arr[posicion] != null) {
            if (arr[posicion] != null) {
                if (arr[posicion].getClaveInt() == clave) {
                    this.arr[posicion].setClaveInt(-1);
                    this.arr[posicion].setClaveString("");
                    cantidadElementos--;
                }
            }
            colisiones++;
            posicion = hashFunc1(clave, colisiones);

        }

    }

    public void remove(String clave) {
        int hash = stringHash(clave, capacidad);
        int colisiones = 0;
        int posicion = hashFunc1(hash, colisiones);
        while (arr[posicion] != null) {
            if (arr[posicion] != null) {
                if (arr[posicion].getClaveString().equals(clave)) {
                    this.arr[posicion].setClaveInt(-1);
                    this.arr[posicion].setClaveString("");
                    cantidadElementos--;
                }
            }
            colisiones++;
            posicion = hashFunc1(hash, colisiones);

        }

    }

    public boolean containsKey(int clave) {
        int colisiones = 0;
        int posicion = hashFunc1(clave, colisiones);
        while (arr[posicion] != null) {

            if (arr[posicion].getClaveInt() == clave) {
                return true;
            }

            colisiones++;
            posicion = hashFunc1(clave, colisiones);

        }

        return false;
    }

    public boolean containsKey(String clave) {
        int hash = stringHash(clave, this.capacidad);
        int colisiones = 0;
        int posicion = hashFunc1(hash, colisiones);
        while (arr[posicion] != null) {

            if (arr[posicion].getClaveString().equals(clave)) {
                return true;
            }

            colisiones++;
            posicion = hashFunc1(hash, colisiones);

        }

        return false;
    }

    public void put(T valor, String clave) {
        if ((double) cantidadElementos / (double) capacidad > 0.75) {
            reHash();
        }
        NodoHash tmp = new NodoHash(valor, clave, this.capacidad);

        int hash = stringHash(clave, this.capacidad);
        int colisiones = 0;
        int posicion = hashFunc1(hash, colisiones);
        while (arr[posicion] != null) {
            if (arr[posicion].esVacio()) {
                break;
            }
            colisiones++;
            posicion = hashFunc1(hash, colisiones);
        }

        cantidadElementos++;
        arr[posicion] = tmp;

    }

    public void put(T valor, int clave) {
        if ((double) cantidadElementos / (double) capacidad > 0.75) {
            reHash();
        }
        NodoHash tmp = new NodoHash(valor, clave);

        int colisiones = 0;
        int posicion = hashFunc1(clave, colisiones);
        while (arr[posicion] != null) {
            if (arr[posicion].esVacio()) {
                break;
            }
            colisiones++;
            posicion = hashFunc1(clave, colisiones);
        }

        cantidadElementos++;
        arr[posicion] = tmp;
    }

    public T get(int clave) {
        int colisiones = 0;
        int posicion = hashFunc1(clave, colisiones);
        while (arr[posicion] != null) {
            if (arr[posicion] != null) {
                if (arr[posicion].getClaveInt() == clave) {
                    return arr[posicion].getValor();
                }
            }
            colisiones++;
            posicion = hashFunc1(clave, colisiones);

        }

        return null;
    }

    public T get(String clave) {

        int hash = stringHash(clave, capacidad);
        int colisiones = 0;
        int posicion = hashFunc1(hash, colisiones);
        while (arr[posicion] != null) {
            if (arr[posicion].getClaveString().equals(clave)) {
                return arr[posicion].getValor();
            }
            colisiones++;
            posicion = hashFunc1(hash, colisiones);
            
        }

        return null;

    }

    private int hashFunc1(int clave, int colisiones) {
        int resultadoHash1 = ((clave % capacidad) + f(colisiones, clave)) % capacidad;
        return resultadoHash1;
    }

    private int f(int i, int clave) {
        int resultadoF = i * hashFunc2(clave);
        return resultadoF;
    }

    private int hashFunc2(int clave) {
        int r = anteriorPrimo(capacidad);
        int resultadoHash2 = r - (clave % r);
        return resultadoHash2;
    }

    public static int stringHash(String clave, int capacidadTabla) {
        int hash = 0;

        for (int i = 0; i < clave.length(); i++) {
            hash += clave.charAt(i);
        }

        int resultadoString = hash % capacidadTabla;

        return resultadoString;
    }

    private static int siguientePrimo(int n) {
        if (n % 2 == 0) {
            n++;
        }
        for (; !esPrimo(n); n += 2);

        return n;
    }

    public static int anteriorPrimo(int n) {
        if (n % 2 == 0) {
            n--;
        } else {
            n -= 2;
        }

        for (; !esPrimo(n); n -= 2);

        return n;
    }

    private static boolean esPrimo(int n) {
        if (n == 2 || n == 3) {
            return true;
        }
        if (n == 1 || n % 2 == 0) {
            return false;
        }
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public T[] values(Class clase) {
        T[] valores = (T[]) Array.newInstance(clase, cantidadElementos);
        int j = 0;
        for (int i = 0; i < capacidad; i++) {
            if (arr[i] != null) {
                if (!arr[i].esVacio()) {
                    valores[j] = arr[i].getValor();
                    j++;
                }
            }

        }
        return valores;
    }
    
    public boolean isEmpty(){
        return cantidadElementos == 0;
    }

    public int getCantidadElementos() {
        return cantidadElementos;
    }

}

