/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic;

import data.Proyecto;
import java.util.Arrays;

/**
 *
 * @author angel
 */
public class MaxHeap {
    
    private Proyecto[] Heap;
    private int size;
    private int maxsize;
    
    private int cantidadProyectos;

    private static final int FRONT = 1;

    public MaxHeap() {
        this.maxsize = 3;
        this.size = 0;
        Heap = new Proyecto[this.maxsize + 1];
        cantidadProyectos = Main.getProyectos().getCantidadElementos();
        Heap[0] = new Proyecto(Integer.MAX_VALUE, ++cantidadProyectos);
        for (int i = 1; i <= maxsize; i++) {
            Heap[i] = new Proyecto(0, ++cantidadProyectos);
        }
    }

    public Proyecto[] getHeap() {
        return Heap;
    }
    
    private int parent(int pos) {
        return pos / 2;
    }

    private int leftChild(int pos) {
        return (2 * pos);
    }

    private int rightChild(int pos) {
        return (2 * pos) + 1;
    }

    public boolean isLeaf(int pos) {
        return pos > (size / 2) && pos <= size;
    }

    private void swap(int fpos, int spos) {
        Proyecto tmp = Heap[fpos];
        Heap[fpos] = Heap[spos];
        Heap[spos] = tmp;
    }

    private void maxHeapify(int pos) {
        if(size == 0){
            return;
        }
        if (!isLeaf(pos)) {
            if (leftChild(pos) <= size) {
                if (rightChild(pos) <= size) {
                    if (Heap[pos].getLikes() < Heap[leftChild(pos)].getLikes() || Heap[pos].getLikes() < Heap[rightChild(pos)].getLikes()) {
                        if (Heap[leftChild(pos)].getLikes() > Heap[rightChild(pos)].getLikes()) {
                            swap(pos, leftChild(pos));
                            maxHeapify(leftChild(pos));
                        } else {
                            swap(pos, rightChild(pos));
                            maxHeapify(rightChild(pos));
                        }
                    }
                } else if (Heap[pos].getLikes() < Heap[leftChild(pos)].getLikes()) {
                    swap(pos, leftChild(pos));
                    maxHeapify(leftChild(pos));
                }
            } else if (rightChild(pos) <= size) {
                if (Heap[pos].getLikes() < Heap[rightChild(pos)].getLikes()) {
                    swap(pos, rightChild(pos));
                    maxHeapify(rightChild(pos));
                }
            }
        }
    }

    public void insert(Proyecto element) {
        checkSize();
        Heap[++size] = element;
        int current = size;
        while (Heap[current].getLikes() > Heap[parent(current)].getLikes()) {
            swap(current, parent(current));
            current = parent(current);
        }
    }
    
    private void checkSize() {
        if (size == maxsize - 1) {
            Heap = Arrays.copyOf(Heap, maxsize * 2 + 1);
            maxsize = maxsize * 2 + 1;
            for (int i = 0; i < Heap.length; i++) {
                if(i > size){
                    Heap[i] = new Proyecto(0, ++cantidadProyectos);
                }
            }
        }
    }
    
    public void print() {
        for (int i = 1; i <= size / 2; i++) {
            System.out.print(
                    " valor padre : " + Heap[i].getLikes() + " hijo izquierdo : " + Heap[2 * i].getLikes()
                    + " hijo derecho :" + Heap[2 * i + 1].getLikes());
            System.out.println();
        }
    }

    public void maxHeap() {
        for (int pos = (size / 2); pos >= 1; pos--) {
            maxHeapify(pos);
        }
    }

    public Proyecto extractMax() {
        Proyecto popped = Heap[FRONT];
        Heap[FRONT] = Heap[size];
        Heap[size] = new Proyecto(0, ++cantidadProyectos);
        size--;
        maxHeapify(FRONT);
        return popped;
    }
    
    public Proyecto remove(int pos){
        Heap[pos] = new Proyecto(Integer.MAX_VALUE,0);
        siftUp(pos);
        return extractMax();
    }
    
    public void siftUp(int pos){
        while ( pos > 1 && Heap[parent(pos)].getLikes() < Heap[pos].getLikes()) {            
            swap(parent(pos),pos);
            pos = parent(pos);
        }
    }
    
    public void changePriority(int pos, Proyecto valor){
        Proyecto oldPos = Heap[pos];
        Heap[pos] = valor;
        if(valor.getLikes() > oldPos.getLikes()){
            siftUp(pos);
        }else{
            maxHeapify(pos);
        }
    }

    public int getSize() {
        return size;
    }
    
}
