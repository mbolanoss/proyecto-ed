package gui;

import businessLogic.DynamicArray;
import businessLogic.LinkedList;
import businessLogic.Main;
import businessLogic.Nodo;
import data.Inversionista;
import data.Proyecto;
import java.awt.Cursor;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author migue
 */
public class ProyectosInteresadosInver extends javax.swing.JFrame {

    /**
     * Creates new form CrearProyecto
     */
    private LinkedList<Proyecto> listaProyectos;
    private DynamicArray<Proyecto> arrProyectos;

    private ArrayList<LinkedList<Proyecto>> paginas;
    private int paginaActual;

    private ArrayList<Integer> idProyectos;

    public ProyectosInteresadosInver(LinkedList<Proyecto> lista) {//Costructor con lista enlazada como parametro
        this.idProyectos = new ArrayList<>();
        this.paginaActual = 0;
        this.listaProyectos = lista;
        this.paginas = new ArrayList<>((int) Math.ceil(listaProyectos.size() / 4));

        initComponents();

        if (this.listaProyectos.isEmpty()) {
            JOptionPane.showMessageDialog(null, "No hay proyectos", "", JOptionPane.INFORMATION_MESSAGE);
        } else {
            configPaginasLinked();
            limpiarInfo();
            llenarPagina(0);
        }

        setLocationRelativeTo(null);

    }

    public ProyectosInteresadosInver(DynamicArray<Proyecto> arr) {//Costructor con lista enlazada como parametro
        this.idProyectos = new ArrayList<>();
        this.paginaActual = 0;
        this.arrProyectos = arr;
        this.paginas = new ArrayList<>((int) Math.ceil(arrProyectos.getSize() / 4));

        initComponents();

        if (this.arrProyectos.isEmpty()) {
            JOptionPane.showMessageDialog(null, "No hay proyectos", "", JOptionPane.INFORMATION_MESSAGE);
        } else {
            configPaginasArray();
            limpiarInfo();
            llenarPagina(0);
        }

        setLocationRelativeTo(null);

    }

    private void configPaginasLinked() {
        this.paginas = new ArrayList<>();//Se borran las paginas previamente creadas
        Nodo<Proyecto> temp = this.listaProyectos.getHead().getNext();
        while (temp != null) {
            LinkedList<Proyecto> listaPagina = new LinkedList<>();
            for (int i = 0; i < 4; i++) {//Se llena cada pagina con maximo 4 proyectos
                if (temp == null) {//Esto por si no hay mas proyectos en la lista
                    break;
                }
                listaPagina.pushFront(temp.getKey());
                temp = temp.getNext();
            }
            this.paginas.add(listaPagina);//Se añade la pagina a la lista de paginas 
        }
    }

    private void configPaginasArray() {
        this.paginas = new ArrayList<>();
        Proyecto[] proyectos = this.arrProyectos.getArray();
        int i = 0;
        while (i < this.arrProyectos.getSize()) {
            LinkedList<Proyecto> listaPagina = new LinkedList<>();
            for (int j = 0; j < 4; j++) {
                if (i == arrProyectos.getSize()) {
                    break;
                }
                listaPagina.pushFront(this.arrProyectos.get(i));
                i++;
            }
            this.paginas.add(listaPagina);
        }
    }

    private void llenarPagina(int numPagina) {
        LinkedList<Proyecto> pagina = this.paginas.get(numPagina);//Se obtiene la lista de proyectos de la pagina actual
        int tamanoPagina = pagina.getSize();
        Nodo<Proyecto> temp = pagina.getHead().getNext();
        Proyecto p = temp.getKey();

        if (tamanoPagina >= 1) {
            jlNombreP1.setText(p.getNombre());
            jlCategoriaP1.setText(p.getCategoria());
            jtxtaDesc1.setText(p.getDescripcion());
            if (this.idProyectos.contains(p.getId())) {
                jlSelectedP1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart_selected.png")));
            }
        }

        if (tamanoPagina >= 2) {
            temp = temp.getNext();
            p = temp.getKey();
            jlNombreP2.setText(p.getNombre());
            jlCategoriaP2.setText(p.getCategoria());
            jtxtaDesc2.setText(p.getDescripcion());
            if (this.idProyectos.contains(p.getId())) {
                jlSelectedP2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart_selected.png")));
            }
        }

        if (tamanoPagina >= 3) {
            temp = temp.getNext();
            p = temp.getKey();
            jlNombreP3.setText(p.getNombre());
            jlCategoriaP3.setText(p.getCategoria());
            jtxtaDesc3.setText(p.getDescripcion());
            if (this.idProyectos.contains(p.getId())) {
                jlSelectedP3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart_selected.png")));
            }
        }

        if (tamanoPagina >= 4) {
            temp = temp.getNext();
            p = temp.getKey();
            jlNombreP4.setText(p.getNombre());
            jlCategoriaP4.setText(p.getCategoria());
            jtxtaDesc4.setText(p.getDescripcion());
            if (this.idProyectos.contains(p.getId())) {
                jlSelectedP4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart_selected.png")));
            }
        }

    }

    private void llenarPaginaOriginal(int numPagina) {
        LinkedList<Proyecto> pagina = this.paginas.get(numPagina);//Se obtiene la lista de proyectos de la pagina actual
        int tamanoPagina = pagina.getSize();
        Nodo<Proyecto> temp = pagina.getHead().getNext();
        Proyecto p = temp.getKey();

        if (tamanoPagina >= 1) {
            jlNombreP1.setText(p.getNombre());
            jlCategoriaP1.setText(p.getCategoria());
            jtxtaDesc1.setText(p.getDescripcion());
            if (this.idProyectos.contains(p.getId())) {
                jlSelectedP1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart_selected.png")));
            }
        }

        if (tamanoPagina >= 2) {
            temp = temp.getNext();
            p = temp.getKey();
            jlNombreP2.setText(p.getNombre());
            jlCategoriaP2.setText(p.getCategoria());
            jtxtaDesc2.setText(p.getDescripcion());
            if (this.idProyectos.contains(p.getId())) {
                jlSelectedP2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart_selected.png")));
            }
        }

        if (tamanoPagina >= 3) {
            temp = temp.getNext();
            p = temp.getKey();
            jlNombreP3.setText(p.getNombre());
            jlCategoriaP3.setText(p.getCategoria());
            jtxtaDesc3.setText(p.getDescripcion());
            if (this.idProyectos.contains(p.getId())) {
                jlSelectedP3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart_selected.png")));
            }
        }

        if (tamanoPagina >= 4) {
            temp = temp.getNext();
            p = temp.getKey();
            jlNombreP4.setText(p.getNombre());
            jlCategoriaP4.setText(p.getCategoria());
            jtxtaDesc4.setText(p.getDescripcion());
            if (this.idProyectos.contains(p.getId())) {
                jlSelectedP4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart_selected.png")));
            }
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jlTitulo = new javax.swing.JLabel();
        jlSalir = new javax.swing.JLabel();
        jlVolver = new javax.swing.JLabel();
        jpLista = new javax.swing.JPanel();
        jlCategoriaP1 = new javax.swing.JLabel();
        jlCategoriaP2 = new javax.swing.JLabel();
        jlCategoriaP3 = new javax.swing.JLabel();
        jlCategoriaP4 = new javax.swing.JLabel();
        jlNombreP2 = new javax.swing.JLabel();
        jlNombreP1 = new javax.swing.JLabel();
        jlNombreP3 = new javax.swing.JLabel();
        jlNombreP4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtxtaDesc1 = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        jtxtaDesc2 = new javax.swing.JTextArea();
        jScrollPane3 = new javax.swing.JScrollPane();
        jtxtaDesc3 = new javax.swing.JTextArea();
        jScrollPane4 = new javax.swing.JScrollPane();
        jtxtaDesc4 = new javax.swing.JTextArea();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jlSelectedP2 = new javax.swing.JLabel();
        jlSelectedP4 = new javax.swing.JLabel();
        jlSelectedP1 = new javax.swing.JLabel();
        jlSelectedP3 = new javax.swing.JLabel();
        jlProjectIcon4 = new javax.swing.JLabel();
        jlProjectIcon1 = new javax.swing.JLabel();
        jlProjectIcon2 = new javax.swing.JLabel();
        jlProjectIcon3 = new javax.swing.JLabel();
        jlPrev = new javax.swing.JLabel();
        jlNext = new javax.swing.JLabel();
        jpBuscar = new javax.swing.JPanel();
        jlSearch = new javax.swing.JLabel();
        jtxtSearch = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(252, 201, 61));

        jlTitulo.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jlTitulo.setForeground(new java.awt.Color(255, 255, 255));
        jlTitulo.setText("Proyectos que te pueden interesar:");

        jlSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/x-mark-48.png"))); // NOI18N
        jlSalir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlSalirMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jlSalirMouseEntered(evt);
            }
        });

        jlVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/arrow-19-48.png"))); // NOI18N
        jlVolver.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlVolverMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jlVolverMouseEntered(evt);
            }
        });

        jpLista.setBackground(new java.awt.Color(255, 255, 255));
        jpLista.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jlCategoriaP1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlCategoriaP1.setForeground(new java.awt.Color(252, 201, 61));
        jlCategoriaP1.setText("Categoria 1");
        jpLista.add(jlCategoriaP1, new org.netbeans.lib.awtextra.AbsoluteConstraints(429, 19, -1, -1));

        jlCategoriaP2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlCategoriaP2.setForeground(new java.awt.Color(252, 201, 61));
        jlCategoriaP2.setText("Categoria 2");
        jpLista.add(jlCategoriaP2, new org.netbeans.lib.awtextra.AbsoluteConstraints(428, 134, -1, -1));

        jlCategoriaP3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlCategoriaP3.setForeground(new java.awt.Color(252, 201, 61));
        jlCategoriaP3.setText("Categoria 3");
        jpLista.add(jlCategoriaP3, new org.netbeans.lib.awtextra.AbsoluteConstraints(429, 237, -1, -1));

        jlCategoriaP4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlCategoriaP4.setForeground(new java.awt.Color(252, 201, 61));
        jlCategoriaP4.setText("Categoria 4");
        jpLista.add(jlCategoriaP4, new org.netbeans.lib.awtextra.AbsoluteConstraints(432, 346, -1, -1));

        jlNombreP2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlNombreP2.setText("Nombre 2");
        jpLista.add(jlNombreP2, new org.netbeans.lib.awtextra.AbsoluteConstraints(111, 134, -1, -1));

        jlNombreP1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlNombreP1.setText("Nombre 1");
        jpLista.add(jlNombreP1, new org.netbeans.lib.awtextra.AbsoluteConstraints(111, 19, -1, -1));

        jlNombreP3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlNombreP3.setText("Nombre 3");
        jpLista.add(jlNombreP3, new org.netbeans.lib.awtextra.AbsoluteConstraints(112, 237, -1, -1));

        jlNombreP4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jlNombreP4.setText("Nombre 4");
        jpLista.add(jlNombreP4, new org.netbeans.lib.awtextra.AbsoluteConstraints(115, 346, -1, -1));

        jtxtaDesc1.setEditable(false);
        jtxtaDesc1.setColumns(20);
        jtxtaDesc1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jtxtaDesc1.setForeground(new java.awt.Color(0, 102, 102));
        jtxtaDesc1.setLineWrap(true);
        jtxtaDesc1.setRows(5);
        jtxtaDesc1.setText("Descripcion 1");
        jtxtaDesc1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane1.setViewportView(jtxtaDesc1);

        jpLista.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(111, 54, 458, 55));

        jtxtaDesc2.setEditable(false);
        jtxtaDesc2.setColumns(20);
        jtxtaDesc2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jtxtaDesc2.setForeground(new java.awt.Color(0, 102, 102));
        jtxtaDesc2.setLineWrap(true);
        jtxtaDesc2.setRows(5);
        jtxtaDesc2.setText("Descripcion 2\n");
        jtxtaDesc2.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane2.setViewportView(jtxtaDesc2);

        jpLista.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(111, 156, 458, 54));

        jtxtaDesc3.setEditable(false);
        jtxtaDesc3.setColumns(20);
        jtxtaDesc3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jtxtaDesc3.setForeground(new java.awt.Color(0, 102, 102));
        jtxtaDesc3.setLineWrap(true);
        jtxtaDesc3.setRows(5);
        jtxtaDesc3.setText("Descripcion 3\n\n");
        jtxtaDesc3.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane3.setViewportView(jtxtaDesc3);

        jpLista.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(112, 260, 458, 59));

        jtxtaDesc4.setEditable(false);
        jtxtaDesc4.setColumns(20);
        jtxtaDesc4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jtxtaDesc4.setForeground(new java.awt.Color(0, 102, 102));
        jtxtaDesc4.setLineWrap(true);
        jtxtaDesc4.setRows(5);
        jtxtaDesc4.setText("Descripcion 4\n\n");
        jtxtaDesc4.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane4.setViewportView(jtxtaDesc4);

        jpLista.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(115, 369, 458, 59));
        jpLista.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 614, 10));
        jpLista.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 221, 614, 10));
        jpLista.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 330, 614, 10));

        jlSelectedP2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart_not_selected.png"))); // NOI18N
        jlSelectedP2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlSelectedP2MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jlSelectedP2MouseEntered(evt);
            }
        });
        jpLista.add(jlSelectedP2, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 170, -1, -1));

        jlSelectedP4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart_not_selected.png"))); // NOI18N
        jlSelectedP4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlSelectedP4MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jlSelectedP4MouseEntered(evt);
            }
        });
        jpLista.add(jlSelectedP4, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 390, -1, -1));

        jlSelectedP1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart_not_selected.png"))); // NOI18N
        jlSelectedP1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlSelectedP1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jlSelectedP1MouseEntered(evt);
            }
        });
        jpLista.add(jlSelectedP1, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 70, -1, -1));

        jlSelectedP3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart_not_selected.png"))); // NOI18N
        jlSelectedP3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlSelectedP3MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jlSelectedP3MouseEntered(evt);
            }
        });
        jpLista.add(jlSelectedP3, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 280, -1, -1));

        jlProjectIcon4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/project_icon.png"))); // NOI18N
        jpLista.add(jlProjectIcon4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 360, -1, -1));

        jlProjectIcon1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/project_icon.png"))); // NOI18N
        jpLista.add(jlProjectIcon1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, -1, -1));

        jlProjectIcon2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/project_icon.png"))); // NOI18N
        jpLista.add(jlProjectIcon2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, -1, -1));

        jlProjectIcon3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/project_icon.png"))); // NOI18N
        jpLista.add(jlProjectIcon3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 250, -1, -1));

        jlPrev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/arrow_prev.png"))); // NOI18N
        jlPrev.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlPrevMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jlPrevMouseEntered(evt);
            }
        });

        jlNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/arrow_next.png"))); // NOI18N
        jlNext.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlNextMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jlNextMouseEntered(evt);
            }
        });

        jpBuscar.setBackground(new java.awt.Color(255, 255, 255));
        jpBuscar.setPreferredSize(new java.awt.Dimension(100, 46));
        jpBuscar.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jlSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/zoom.png"))); // NOI18N
        jlSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlSearchMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jlSearchMouseEntered(evt);
            }
        });
        jpBuscar.add(jlSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(163, 11, -1, -1));

        jtxtSearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jtxtSearch.setToolTipText("Busca tus proyectos por el nombre");
        jtxtSearch.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jtxtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtxtSearchActionPerformed(evt);
            }
        });
        jpBuscar.add(jtxtSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 11, 143, -1));
        jpBuscar.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 140, 10));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(78, 78, 78)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jpBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jlPrev)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jlNext))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jlTitulo)
                        .addComponent(jpLista, javax.swing.GroupLayout.PREFERRED_SIZE, 638, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(84, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jlSalir)
                .addGap(48, 48, 48))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(38, 38, 38)
                    .addComponent(jlVolver)
                    .addContainerGap(714, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jlSalir)
                .addGap(45, 45, 45)
                .addComponent(jlTitulo)
                .addGap(18, 18, 18)
                .addComponent(jpLista, javax.swing.GroupLayout.PREFERRED_SIZE, 439, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jpBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlPrev)
                    .addComponent(jlNext))
                .addContainerGap(69, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(38, 38, 38)
                    .addComponent(jlVolver)
                    .addContainerGap(664, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jlVolverMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlVolverMouseEntered
        // TODO add your handling code here:
        jlVolver.setCursor(new Cursor(HAND_CURSOR));
    }//GEN-LAST:event_jlVolverMouseEntered

    private void jlSalirMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlSalirMouseEntered
        // TODO add your handling code here:
        jlSalir.setCursor(new Cursor(HAND_CURSOR));
    }//GEN-LAST:event_jlSalirMouseEntered

    private void jlSalirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlSalirMouseClicked
        // TODO add your handling code here:
        int respuesta = JOptionPane.showConfirmDialog(null, "¿Estás seguro?");
        if(respuesta == 0){
            Main.guardarDatos();
            System.exit(0);
        }
    }//GEN-LAST:event_jlSalirMouseClicked

    private void jlPrevMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlPrevMouseEntered
        // TODO add your handling code here:
        jlPrev.setCursor(new Cursor(HAND_CURSOR));
    }//GEN-LAST:event_jlPrevMouseEntered

    private void jlNextMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlNextMouseEntered
        // TODO add your handling code here:
        jlNext.setCursor(new Cursor(HAND_CURSOR));
    }//GEN-LAST:event_jlNextMouseEntered

    private void jlSelectedP3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlSelectedP3MouseEntered
        // TODO add your handling code here:
        jlSelectedP3.setCursor(new Cursor(HAND_CURSOR));
    }//GEN-LAST:event_jlSelectedP3MouseEntered

    private void jlSelectedP1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlSelectedP1MouseEntered
        // TODO add your handling code here:
        jlSelectedP1.setCursor(new Cursor(HAND_CURSOR));
    }//GEN-LAST:event_jlSelectedP1MouseEntered

    private void jlSelectedP2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlSelectedP2MouseEntered
        // TODO add your handling code here:
        jlSelectedP2.setCursor(new Cursor(HAND_CURSOR));
    }//GEN-LAST:event_jlSelectedP2MouseEntered

    private void jlSelectedP4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlSelectedP4MouseEntered
        // TODO add your handling code here:
        jlSelectedP4.setCursor(new Cursor(HAND_CURSOR));
    }//GEN-LAST:event_jlSelectedP4MouseEntered

    private void jlSelectedP1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlSelectedP1MouseClicked
        // TODO add your handling code here:
        if (this.arrProyectos == null) {
            if (!this.listaProyectos.isEmpty()) {
                Nodo<Proyecto> temp = this.paginas.get(this.paginaActual).getHead().getNext();
                Proyecto p = temp.getKey();
                String ruta = jlSelectedP1.getIcon().toString();
                if (ruta.equals("file:/C:/Users/migue/Documents/NetBeansProjects/proyecto-ed/Proyecto%20ED/build/classes/resources/heart_selected.png")) {//El usuario quita el me gusta del proyecto
                    jlSelectedP1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/hearthueco.png")));
                    this.idProyectos.remove(new Integer(p.getId()));

                } else {
                    jlSelectedP1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart_selected.png")));//El usuario le da me gusta al proyecto
                    this.idProyectos.add(p.getId());

                }
            }
        } else {
            if (!this.arrProyectos.isEmpty()) {
                Nodo<Proyecto> temp = this.paginas.get(this.paginaActual).getHead().getNext();
                Proyecto p = temp.getKey();
                String ruta = jlSelectedP1.getIcon().toString();
                if (ruta.equals("file:/C:/Users/migue/Documents/NetBeansProjects/proyecto-ed/Proyecto%20ED/build/classes/resources/heart_selected.png")) {//El usuario quita el me gusta del proyecto
                    jlSelectedP1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/hearthueco.png")));
                    this.idProyectos.remove(new Integer(p.getId()));

                } else {
                    jlSelectedP1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart_selected.png")));//El usuario le da me gusta al proyecto
                    this.idProyectos.add(p.getId());

                }
            }
        }


    }//GEN-LAST:event_jlSelectedP1MouseClicked

    private void jlSelectedP2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlSelectedP2MouseClicked
        // TODO add your handling code here:
        if (this.arrProyectos == null) {
            if (!this.listaProyectos.isEmpty()) {
                Nodo<Proyecto> temp = this.paginas.get(this.paginaActual).getHead().getNext().getNext();
                Proyecto p = temp.getKey();
                String ruta = jlSelectedP2.getIcon().toString();
                if (ruta.equals("file:/C:/Users/migue/Documents/NetBeansProjects/proyecto-ed/Proyecto%20ED/build/classes/resources/heart_selected.png")) {//El usuario quita el me gusta del proyecto
                    jlSelectedP2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/hearthueco.png")));
                    this.idProyectos.remove(new Integer(p.getId()));
                } else {
                    jlSelectedP2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart_selected.png")));//El usuario le da me gusta al proyecto
                    this.idProyectos.add(p.getId());
                }
            }
        } else {
            if (!this.arrProyectos.isEmpty()) {
                Nodo<Proyecto> temp = this.paginas.get(this.paginaActual).getHead().getNext().getNext();
                Proyecto p = temp.getKey();
                String ruta = jlSelectedP2.getIcon().toString();
                if (ruta.equals("file:/C:/Users/migue/Documents/NetBeansProjects/proyecto-ed/Proyecto%20ED/build/classes/resources/heart_selected.png")) {//El usuario quita el me gusta del proyecto
                    jlSelectedP2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/hearthueco.png")));
                    this.idProyectos.remove(new Integer(p.getId()));
                } else {
                    jlSelectedP2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart_selected.png")));//El usuario le da me gusta al proyecto
                    this.idProyectos.add(p.getId());
                }
            }
        }


    }//GEN-LAST:event_jlSelectedP2MouseClicked

    private void jlSelectedP3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlSelectedP3MouseClicked
        // TODO add your handling code here:
        if (this.arrProyectos == null) {
            if (!this.listaProyectos.isEmpty()) {
                Nodo<Proyecto> temp = this.paginas.get(this.paginaActual).getHead().getNext().getNext().getNext();
                Proyecto p = temp.getKey();
                String ruta = jlSelectedP3.getIcon().toString();
                if (ruta.equals("file:/C:/Users/migue/Documents/NetBeansProjects/proyecto-ed/Proyecto%20ED/build/classes/resources/heart_selected.png")) {//El usuario quita el me gusta del proyecto
                    jlSelectedP3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/hearthueco.png")));
                    this.idProyectos.remove(new Integer(p.getId()));
                } else {
                    jlSelectedP3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart_selected.png")));//El usuario le da me gusta al proyecto
                    this.idProyectos.add(p.getId());
                }
            }
        } else {
            if (!this.arrProyectos.isEmpty()) {
                Nodo<Proyecto> temp = this.paginas.get(this.paginaActual).getHead().getNext().getNext().getNext();
                Proyecto p = temp.getKey();
                String ruta = jlSelectedP3.getIcon().toString();
                if (ruta.equals("file:/C:/Users/migue/Documents/NetBeansProjects/proyecto-ed/Proyecto%20ED/build/classes/resources/heart_selected.png")) {//El usuario quita el me gusta del proyecto
                    jlSelectedP3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/hearthueco.png")));
                    this.idProyectos.remove(new Integer(p.getId()));
                } else {
                    jlSelectedP3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart_selected.png")));//El usuario le da me gusta al proyecto
                    this.idProyectos.add(p.getId());
                }
            }
        }


    }//GEN-LAST:event_jlSelectedP3MouseClicked

    private void jlSelectedP4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlSelectedP4MouseClicked
        // TODO add your handling code here:
        if (this.arrProyectos == null) {
            if (!this.listaProyectos.isEmpty()) {
                Nodo<Proyecto> temp = this.paginas.get(this.paginaActual).getHead().getNext().getNext().getNext().getNext();
                Proyecto p = temp.getKey();
                String ruta = jlSelectedP4.getIcon().toString();
                if (ruta.equals("file:/C:/Users/migue/Documents/NetBeansProjects/proyecto-ed/Proyecto%20ED/build/classes/resources/heart_selected.png")) {//El usuario quita el me gusta del proyecto
                    jlSelectedP4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/hearthueco.png")));
                    this.idProyectos.remove(new Integer(p.getId()));
                } else {
                    jlSelectedP4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart_selected.png")));//El usuario le da me gusta al proyecto
                    this.idProyectos.add(p.getId());
                }
            }
        } else {
            if (!this.arrProyectos.isEmpty()) {
                Nodo<Proyecto> temp = this.paginas.get(this.paginaActual).getHead().getNext().getNext().getNext().getNext();
                Proyecto p = temp.getKey();
                String ruta = jlSelectedP4.getIcon().toString();
                if (ruta.equals("file:/C:/Users/migue/Documents/NetBeansProjects/proyecto-ed/Proyecto%20ED/build/classes/resources/heart_selected.png")) {//El usuario quita el me gusta del proyecto
                    jlSelectedP4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/hearthueco.png")));
                    this.idProyectos.remove(new Integer(p.getId()));
                } else {
                    jlSelectedP4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart_selected.png")));//El usuario le da me gusta al proyecto
                    this.idProyectos.add(p.getId());
                }
            }
        }


    }//GEN-LAST:event_jlSelectedP4MouseClicked

    private void limpiarInfo() {
        jlNombreP1.setText("");
        jlCategoriaP1.setText("");
        jtxtaDesc1.setText("");
        jlSelectedP1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/hearthueco.png")));

        jlNombreP2.setText("");
        jlCategoriaP2.setText("");
        jtxtaDesc2.setText("");
        jlSelectedP2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/hearthueco.png")));

        jlNombreP3.setText("");
        jlCategoriaP3.setText("");
        jtxtaDesc3.setText("");
        jlSelectedP3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/hearthueco.png")));

        jlNombreP4.setText("");
        jlCategoriaP4.setText("");
        jtxtaDesc4.setText("");
        jlSelectedP4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/hearthueco.png")));
    }

    private void jlPrevMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlPrevMouseClicked
        // TODO add your handling code here:
        if (this.arrProyectos == null) {
            if (!this.listaProyectos.isEmpty()) {
                if (this.paginaActual == 0) {
                    JOptionPane.showMessageDialog(null, "No hay mas proyectos que mostrar");
                } else {
                    this.paginaActual--;
                    limpiarInfo();
                    llenarPagina(this.paginaActual);
                }
            }
        } else {
            if (!this.arrProyectos.isEmpty()) {
                if (this.paginaActual == 0) {
                    JOptionPane.showMessageDialog(null, "No hay mas proyectos que mostrar");
                } else {
                    this.paginaActual--;
                    limpiarInfo();
                    llenarPagina(this.paginaActual);
                }
            }
        }


    }//GEN-LAST:event_jlPrevMouseClicked

    private void jlNextMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlNextMouseClicked
        // TODO add your handling code here:
        if (this.arrProyectos == null) {
            if (!this.listaProyectos.isEmpty()) {
                if (this.paginaActual == this.paginas.size() - 1) {
                    JOptionPane.showMessageDialog(null, "No hay mas proyectos que mostrar");
                } else {
                    this.paginaActual++;
                    limpiarInfo();
                    llenarPagina(this.paginaActual);
                }
            }
        } else {
            if (!this.arrProyectos.isEmpty()) {
                if (this.paginaActual == this.paginas.size() - 1) {
                    JOptionPane.showMessageDialog(null, "No hay mas proyectos que mostrar");
                } else {
                    this.paginaActual++;
                    limpiarInfo();
                    llenarPagina(this.paginaActual);
                }
            }
        }


    }//GEN-LAST:event_jlNextMouseClicked

    private void jlVolverMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlVolverMouseClicked
        // TODO add your handling code here:
        /*for(int i : this.idProyectos){
            System.out.println(i);
        }*/
        if (this.idProyectos.isEmpty()) {
            setVisible(false);
            new MenuInversionista(Main.getUsuarioLogeado()).setVisible(true);
        } else {
            int respuesta = JOptionPane.showConfirmDialog(null, "¿Desea guardar los cambios?");
            if (respuesta == 0) {
                for (int id : this.idProyectos) {//Se agregan los proyectos que el usuario le dio me gusta a su lista de proyectos
                    Main.aumentarLikes(id);
                    Main.getInversionistas().get(Main.getUsuarioLogeado()).getInteresados().pushFront(id);
                }

            }
            setVisible(false);

            Main.login.menuInversionista.actualizarLista(Main.getInversionistas().get(Main.getUsuarioLogeado()).verProyectosInteresados());
            Main.login.menuInversionista.setVisible(true);
            Main.login.menuInversionista.llenarPanel();
        }

    }//GEN-LAST:event_jlVolverMouseClicked

    private void jtxtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtxtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtxtSearchActionPerformed

    private void jlSearchMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlSearchMouseEntered
        // TODO add your handling code here:
        jlSearch.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }//GEN-LAST:event_jlSearchMouseEntered

    private void jlSearchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlSearchMouseClicked
        // TODO add your handling code here:
        String busqueda = jtxtSearch.getText();
        Inversionista inver = Main.getInversionistas().get(Main.getUsuarioLogeado());
        DynamicArray<Proyecto> resultado = inver.buscarProyecto(busqueda, inver.filtrarProyectosArray());

        if (resultado.isEmpty()) {
            JOptionPane.showMessageDialog(null, "No se encontró ningún proyecto", "", JOptionPane.INFORMATION_MESSAGE);
        } else {
            this.arrProyectos = resultado;
            configPaginasArray();
            limpiarInfo();
            this.paginaActual = 0;
            llenarPagina(0);
        }
    }//GEN-LAST:event_jlSearchMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ProyectosInteresadosInver.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ProyectosInteresadosInver.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ProyectosInteresadosInver.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ProyectosInteresadosInver.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        LinkedList<Proyecto> prueba = new LinkedList<>();

        prueba.pushFront(new Proyecto(1, "nombre 1", "desc 1", "categoria 1", 100, "0", "alcance 1"));
        prueba.pushFront(new Proyecto(2, "nombre 2", "desc 2", "categoria 2", 100, "0", "alcance 1"));
        prueba.pushFront(new Proyecto(3, "nombre 3", "desc 3", "categoria 3", 100, "0", "alcance 1"));
        prueba.pushFront(new Proyecto(4, "nombre 4", "desc 4", "categoria 4", 100, "0", "alcance 1"));
        prueba.pushFront(new Proyecto(5, "nombre 5", "desc 5", "categoria 5", 100, "0", "alcance 1"));
        prueba.pushFront(new Proyecto(6, "nombre 6", "desc 6", "categoria 6", 100, "0", "alcance 1"));
        prueba.pushFront(new Proyecto(7, "nombre 7", "desc 7", "categoria 7", 100, "0", "alcance 1"));
        prueba.pushFront(new Proyecto(8, "nombre 8", "desc 8", "categoria 8", 100, "0", "alcance 1"));
        prueba.pushFront(new Proyecto(9, "nombre 9", "desc 9", "categoria 9", 100, "0", "alcance 1"));
        prueba.pushFront(new Proyecto(10, "nombre 10", "desc 10", "categoria 10", 100, "0", "alcance 1"));
        prueba.pushFront(new Proyecto(11, "nombre 11", "desc 11", "categoria 11", 100, "0", "alcance 1"));
        prueba.pushFront(new Proyecto(12, "nombre 12", "desc 12", "categoria 12", 100, "0", "alcance 1"));
        prueba.pushFront(new Proyecto(13, "nombre 13", "desc 13", "categoria 13", 100, "0", "alcance 1"));
        prueba.pushFront(new Proyecto(14, "nombre 14", "desc 14", "categoria 14", 100, "0", "alcance 1"));

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ProyectosInteresadosInver(prueba).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JLabel jlCategoriaP1;
    private javax.swing.JLabel jlCategoriaP2;
    private javax.swing.JLabel jlCategoriaP3;
    private javax.swing.JLabel jlCategoriaP4;
    private javax.swing.JLabel jlNext;
    private javax.swing.JLabel jlNombreP1;
    private javax.swing.JLabel jlNombreP2;
    private javax.swing.JLabel jlNombreP3;
    private javax.swing.JLabel jlNombreP4;
    private javax.swing.JLabel jlPrev;
    private javax.swing.JLabel jlProjectIcon1;
    private javax.swing.JLabel jlProjectIcon2;
    private javax.swing.JLabel jlProjectIcon3;
    private javax.swing.JLabel jlProjectIcon4;
    private javax.swing.JLabel jlSalir;
    private javax.swing.JLabel jlSearch;
    private javax.swing.JLabel jlSelectedP1;
    private javax.swing.JLabel jlSelectedP2;
    private javax.swing.JLabel jlSelectedP3;
    private javax.swing.JLabel jlSelectedP4;
    private javax.swing.JLabel jlTitulo;
    private javax.swing.JLabel jlVolver;
    private javax.swing.JPanel jpBuscar;
    private javax.swing.JPanel jpLista;
    private javax.swing.JTextField jtxtSearch;
    private javax.swing.JTextArea jtxtaDesc1;
    private javax.swing.JTextArea jtxtaDesc2;
    private javax.swing.JTextArea jtxtaDesc3;
    private javax.swing.JTextArea jtxtaDesc4;
    // End of variables declaration//GEN-END:variables

}
