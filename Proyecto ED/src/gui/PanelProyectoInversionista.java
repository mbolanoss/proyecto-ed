/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import businessLogic.Main;
import data.Proyecto;
import java.awt.Cursor;
import java.awt.Dimension;
import javax.swing.ImageIcon;

/**
 *
 * @author angel
 */
public class PanelProyectoInversionista extends javax.swing.JPanel {

    private Proyecto proyecto;

    /**
     * Creates new form PanelProyecto
     */
    public PanelProyectoInversionista(Proyecto p) {
        this.proyecto = p;
        initComponents();
        jLabelNombreProyecto.setText(proyecto.getNombre());
        jLabelCategorias.setText(proyecto.getCategoria());
        jTextAreaDescripcion.setText(proyecto.getDescripcion());
        jTextAreaDescripcion.setEditable(false);
        Dimension dimensionPaneProyecto = new Dimension(968, 120);
        setSize(dimensionPaneProyecto);
        setMaximumSize(dimensionPaneProyecto);
        setMinimumSize(dimensionPaneProyecto);
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelCategorias = new javax.swing.JLabel();
        jLabelNombreProyecto = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTextAreaDescripcion = new javax.swing.JTextArea();
        btncorazon = new javax.swing.JButton();
        jlIconoProyecto = new javax.swing.JLabel();
        btnInvertir = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabelCategorias.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelCategorias.setForeground(new java.awt.Color(252, 201, 61));
        jLabelCategorias.setText("Categoria:");

        jLabelNombreProyecto.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelNombreProyecto.setText("Nombre proyecto");

        jSeparator2.setBackground(new java.awt.Color(252, 201, 61));
        jSeparator2.setForeground(new java.awt.Color(252, 201, 60));
        jSeparator2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(252, 201, 61)));
        jSeparator2.setPreferredSize(new java.awt.Dimension(0, 3));

        jTextAreaDescripcion.setColumns(20);
        jTextAreaDescripcion.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTextAreaDescripcion.setRows(5);
        jScrollPane4.setViewportView(jTextAreaDescripcion);

        btncorazon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/heart32.png"))); // NOI18N
        btncorazon.setBorderPainted(false);
        btncorazon.setContentAreaFilled(false);
        btncorazon.setFocusPainted(false);
        btncorazon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btncorazonMouseEntered(evt);
            }
        });
        btncorazon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncorazonActionPerformed(evt);
            }
        });

        jlIconoProyecto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/project_icon.png"))); // NOI18N
        jlIconoProyecto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlIconoProyectoMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jlIconoProyectoMouseEntered(evt);
            }
        });

        btnInvertir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/money.png"))); // NOI18N
        btnInvertir.setBorderPainted(false);
        btnInvertir.setContentAreaFilled(false);
        btnInvertir.setFocusPainted(false);
        btnInvertir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnInvertirMouseEntered(evt);
            }
        });
        btnInvertir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInvertirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelNombreProyecto)
                        .addGap(550, 550, 550)
                        .addComponent(jLabelCategorias))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jlIconoProyecto)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 719, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(btncorazon, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnInvertir, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(25, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 927, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(21, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelCategorias, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabelNombreProyecto))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlIconoProyecto)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(23, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnInvertir)
                            .addComponent(btncorazon))
                        .addGap(38, 38, 38))))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addGap(0, 122, Short.MAX_VALUE)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btncorazonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncorazonActionPerformed
        Main.quitarLike(proyecto.getId());
        Main.inversionistas.get(Main.getUsuarioLogeado()).eliminarProyecto(proyecto.getId());
        
        Main.login.menuInversionista.actualizarLista(Main.getInversionistas().get(Main.getUsuarioLogeado()).verProyectosInteresados());
        Main.login.menuInversionista.llenarPanel();
    }//GEN-LAST:event_btncorazonActionPerformed

    private void btncorazonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncorazonMouseEntered
        // TODO add your handling code here:
        btncorazon.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }//GEN-LAST:event_btncorazonMouseEntered

    private void btnInvertirMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnInvertirMouseEntered
        // TODO add your handling code here:
        btnInvertir.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }//GEN-LAST:event_btnInvertirMouseEntered

    private void btnInvertirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInvertirActionPerformed
        // TODO add your handling code here:
        Main.login.menuInversionista.inversion = new Inversion(proyecto);
        Main.login.menuInversionista.inversion.setVisible(true);
    }//GEN-LAST:event_btnInvertirActionPerformed

    private void jlIconoProyectoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlIconoProyectoMouseEntered
        // TODO add your handling code here:
        jlIconoProyecto.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }//GEN-LAST:event_jlIconoProyectoMouseEntered

    private void jlIconoProyectoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlIconoProyectoMouseClicked
        // TODO add your handling code here:
        Main.login.menuInversionista.setVisible(false);
        new VerProyecto(proyecto).setVisible(true);
    }//GEN-LAST:event_jlIconoProyectoMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnInvertir;
    private javax.swing.JButton btncorazon;
    private javax.swing.JLabel jLabelCategorias;
    private javax.swing.JLabel jLabelNombreProyecto;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextArea jTextAreaDescripcion;
    private javax.swing.JLabel jlIconoProyecto;
    // End of variables declaration//GEN-END:variables
}
