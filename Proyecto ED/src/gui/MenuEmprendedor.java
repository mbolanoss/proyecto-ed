/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import businessLogic.DynamicArray;
import businessLogic.LinkedList;
import businessLogic.Main;
import businessLogic.Nodo;
import data.Emprendedor;
import data.Proyecto;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.ScrollPane;
import java.awt.Toolkit;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author angel
 */
public final class MenuEmprendedor extends javax.swing.JFrame {

    private LinkedList<Proyecto> lista;
    private DynamicArray<Proyecto> arreglo;
    
    public MenuEmprendedor(LinkedList<Proyecto> lista) {
        initComponents();
        setResizable(false);
        Dimension dimension = new Dimension(1549, 750);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(dimension);
        setMaximumSize(dimension);
        setMinimumSize(dimension);
        this.setLocation(
                screenSize.width / 2 - this.getWidth() / 2,
                screenSize.height / 2 - this.getHeight() / 2
        );

        jLabelUsuario.setText(Main.getUsuarioLogeado());
        jLabelNombreUsuario.setText(Main.getEmprendedores().get(Main.getUsuarioLogeado()).getNombre());
        jLabelCorreoUsuario.setText(Main.getEmprendedores().get(Main.getUsuarioLogeado()).getEmail());
        jLabelCiudadUsuario.setText(Main.getEmprendedores().get(Main.getUsuarioLogeado()).getCiudad());

        this.lista = lista;
        actualizarEscroll();

        this.pack();

    }
    
    public MenuEmprendedor(DynamicArray<Proyecto> lista) {
        initComponents();
        setResizable(false);
        Dimension dimension = new Dimension(1549, 750);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(dimension);
        setMaximumSize(dimension);
        setMinimumSize(dimension);
        this.setLocation(
                screenSize.width / 2 - this.getWidth() / 2,
                screenSize.height / 2 - this.getHeight() / 2
        );

        jLabelUsuario.setText(Main.getUsuarioLogeado());
        jLabelNombreUsuario.setText(Main.getEmprendedores().get(Main.getUsuarioLogeado()).getNombre());
        jLabelCorreoUsuario.setText(Main.getEmprendedores().get(Main.getUsuarioLogeado()).getEmail());
        jLabelCiudadUsuario.setText(Main.getEmprendedores().get(Main.getUsuarioLogeado()).getCiudad());

        this.arreglo = lista;
        actualizarEscroll();

        this.pack();

    }
    
    public void actualizarDatos(){
        jLabelUsuario.setText(Main.getUsuarioLogeado());
        System.out.println(Main.getEmprendedores().containsKey(Main.getUsuarioLogeado()));
        jLabelNombreUsuario.setText(Main.getEmprendedores().get(Main.getUsuarioLogeado()).getNombre());
        jLabelCorreoUsuario.setText(Main.getEmprendedores().get(Main.getUsuarioLogeado()).getEmail());
        jLabelCiudadUsuario.setText(Main.getEmprendedores().get(Main.getUsuarioLogeado()).getCiudad());
    }

    MenuEmprendedor() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    public void actualizarEscroll() {
        JPanel jPanel2 = new JPanel();
        jPanel2.setBackground(Color.WHITE);
        jPanel2.setLayout(new BoxLayout(jPanel2, BoxLayout.Y_AXIS));

        if (this.arreglo == null) {
            int tempSize = this.lista.getSize();
            Nodo<Proyecto> temp = this.lista.getHead().getNext();
            while (tempSize != 0) {
                String categoria = "Categoria: ";
                jPanel2.add(new PanelProyectoEmprendedor(temp.getKey()));
                temp = temp.getNext();
                tempSize--;
            }
        }else{
            for(int i = 0; i < this.arreglo.getSize(); i++){
                jPanel2.add(new PanelProyectoEmprendedor(this.arreglo.get(i)));
            }
        }

        jScrollPane2.setBackground(new Color(255, 255, 255));
        jScrollPane2.setViewportView(jPanel2);
        jScrollPane2.updateUI();

        this.pack();
    }

    public LinkedList<Proyecto> getLista() {
        return lista;
    }

    public void setLista(LinkedList<Proyecto> lista) {
        this.lista = lista;
    }
    
    public void setArreglo(DynamicArray<Proyecto> arreglo){
        this.arreglo = arreglo;
    }

    public DynamicArray<Proyecto> getArreglo() {
        return arreglo;
    }

    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanelPrincipal = new javax.swing.JPanel();
        btnsalir = new javax.swing.JButton();
        jPanelInfoUsuario = new javax.swing.JPanel();
        jLabelUsuario = new javax.swing.JLabel();
        jLabelNombreUsuario = new javax.swing.JLabel();
        jLabelCorreoUsuario = new javax.swing.JLabel();
        jLabelCiudadUsuario = new javax.swing.JLabel();
        jPanelEditar = new javax.swing.JPanel();
        btneditar = new javax.swing.JButton();
        btncerrarsesion = new javax.swing.JButton();
        jLabelFotoUsuario = new javax.swing.JLabel();
        jPanelProyectosGeneral = new javax.swing.JPanel();
        jPanelAgregarProyecto = new javax.swing.JPanel();
        btnagregarproyecto = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jpBuscar = new javax.swing.JPanel();
        jlSearch = new javax.swing.JLabel();
        jtxtSearch = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        jPanelTop3 = new javax.swing.JPanel();
        btnTop3 = new javax.swing.JButton();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanelPrincipal.setBackground(new java.awt.Color(252, 201, 61));
        jPanelPrincipal.setName(""); // NOI18N

        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/close.png"))); // NOI18N
        btnsalir.setBorderPainted(false);
        btnsalir.setContentAreaFilled(false);
        btnsalir.setFocusPainted(false);
        btnsalir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnsalirMouseEntered(evt);
            }
        });
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        jPanelInfoUsuario.setBackground(new java.awt.Color(255, 255, 255));
        jPanelInfoUsuario.setPreferredSize(new java.awt.Dimension(350, 550));

        jLabelUsuario.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabelUsuario.setText("felipe92");

        jLabelNombreUsuario.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabelNombreUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/userblacklit.png"))); // NOI18N
        jLabelNombreUsuario.setText(" Juan Felipe Corredor");

        jLabelCorreoUsuario.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabelCorreoUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/mail.png"))); // NOI18N
        jLabelCorreoUsuario.setText(" felipe@unal.edu.co");

        jLabelCiudadUsuario.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabelCiudadUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/pin.png"))); // NOI18N
        jLabelCiudadUsuario.setText("Bogotá");

        jPanelEditar.setBackground(new java.awt.Color(251, 133, 46));

        btneditar.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        btneditar.setForeground(new java.awt.Color(255, 255, 255));
        btneditar.setText("EDITAR");
        btneditar.setBorderPainted(false);
        btneditar.setContentAreaFilled(false);
        btneditar.setDoubleBuffered(true);
        btneditar.setFocusPainted(false);
        btneditar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btneditarMouseEntered(evt);
            }
        });
        btneditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btneditarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelEditarLayout = new javax.swing.GroupLayout(jPanelEditar);
        jPanelEditar.setLayout(jPanelEditarLayout);
        jPanelEditarLayout.setHorizontalGroup(
            jPanelEditarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btneditar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanelEditarLayout.setVerticalGroup(
            jPanelEditarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btneditar, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
        );

        btncerrarsesion.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        btncerrarsesion.setForeground(new java.awt.Color(251, 133, 46));
        btncerrarsesion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/logout.png"))); // NOI18N
        btncerrarsesion.setText("Cerrar sesión");
        btncerrarsesion.setBorderPainted(false);
        btncerrarsesion.setContentAreaFilled(false);
        btncerrarsesion.setFocusPainted(false);
        btncerrarsesion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btncerrarsesionMouseEntered(evt);
            }
        });
        btncerrarsesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncerrarsesionActionPerformed(evt);
            }
        });

        jLabelFotoUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/userblack.png"))); // NOI18N

        javax.swing.GroupLayout jPanelInfoUsuarioLayout = new javax.swing.GroupLayout(jPanelInfoUsuario);
        jPanelInfoUsuario.setLayout(jPanelInfoUsuarioLayout);
        jPanelInfoUsuarioLayout.setHorizontalGroup(
            jPanelInfoUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelInfoUsuarioLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanelInfoUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelCiudadUsuario)
                    .addComponent(jLabelCorreoUsuario)
                    .addGroup(jPanelInfoUsuarioLayout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jPanelEditar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabelNombreUsuario))
                .addGap(30, 30, 30))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelInfoUsuarioLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabelFotoUsuario)
                .addGap(109, 109, 109))
            .addGroup(jPanelInfoUsuarioLayout.createSequentialGroup()
                .addGroup(jPanelInfoUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelInfoUsuarioLayout.createSequentialGroup()
                        .addGap(95, 95, 95)
                        .addComponent(btncerrarsesion))
                    .addGroup(jPanelInfoUsuarioLayout.createSequentialGroup()
                        .addGap(138, 138, 138)
                        .addComponent(jLabelUsuario)))
                .addContainerGap(87, Short.MAX_VALUE))
        );
        jPanelInfoUsuarioLayout.setVerticalGroup(
            jPanelInfoUsuarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelInfoUsuarioLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabelFotoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelUsuario)
                .addGap(32, 32, 32)
                .addComponent(jLabelNombreUsuario)
                .addGap(18, 18, 18)
                .addComponent(jLabelCorreoUsuario)
                .addGap(18, 18, 18)
                .addComponent(jLabelCiudadUsuario)
                .addGap(36, 36, 36)
                .addComponent(jPanelEditar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btncerrarsesion)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanelProyectosGeneral.setBackground(new java.awt.Color(255, 255, 255));
        jPanelProyectosGeneral.setPreferredSize(new java.awt.Dimension(1000, 600));
        jPanelProyectosGeneral.setLayout(null);

        jPanelAgregarProyecto.setBackground(new java.awt.Color(251, 133, 46));

        btnagregarproyecto.setBackground(new java.awt.Color(255, 51, 51));
        btnagregarproyecto.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        btnagregarproyecto.setForeground(new java.awt.Color(255, 255, 255));
        btnagregarproyecto.setText("AGREGAR PROYECTO");
        btnagregarproyecto.setBorderPainted(false);
        btnagregarproyecto.setContentAreaFilled(false);
        btnagregarproyecto.setFocusPainted(false);
        btnagregarproyecto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnagregarproyectoMouseEntered(evt);
            }
        });
        btnagregarproyecto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarproyectoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelAgregarProyectoLayout = new javax.swing.GroupLayout(jPanelAgregarProyecto);
        jPanelAgregarProyecto.setLayout(jPanelAgregarProyectoLayout);
        jPanelAgregarProyectoLayout.setHorizontalGroup(
            jPanelAgregarProyectoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelAgregarProyectoLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnagregarproyecto, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanelAgregarProyectoLayout.setVerticalGroup(
            jPanelAgregarProyectoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnagregarproyecto, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)
        );

        jPanelProyectosGeneral.add(jPanelAgregarProyecto);
        jPanelAgregarProyecto.setBounds(361, 484, 252, 54);

        jScrollPane2.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jPanelProyectosGeneral.add(jScrollPane2);
        jScrollPane2.setBounds(0, 0, 988, 450);

        jpBuscar.setBackground(new java.awt.Color(255, 255, 255));
        jpBuscar.setPreferredSize(new java.awt.Dimension(100, 46));
        jpBuscar.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jlSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/zoom.png"))); // NOI18N
        jlSearch.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlSearchMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jlSearchMouseEntered(evt);
            }
        });
        jpBuscar.add(jlSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(163, 11, -1, -1));

        jtxtSearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jtxtSearch.setToolTipText("Busca tus proyectos por el nombre");
        jtxtSearch.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jtxtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtxtSearchActionPerformed(evt);
            }
        });
        jpBuscar.add(jtxtSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 143, -1));
        jpBuscar.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 140, 10));

        jPanelTop3.setBackground(new java.awt.Color(251, 133, 46));

        btnTop3.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        btnTop3.setForeground(new java.awt.Color(255, 255, 255));
        btnTop3.setText("TOP 3");
        btnTop3.setBorderPainted(false);
        btnTop3.setContentAreaFilled(false);
        btnTop3.setDoubleBuffered(true);
        btnTop3.setFocusPainted(false);
        btnTop3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnTop3MouseEntered(evt);
            }
        });
        btnTop3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTop3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelTop3Layout = new javax.swing.GroupLayout(jPanelTop3);
        jPanelTop3.setLayout(jPanelTop3Layout);
        jPanelTop3Layout.setHorizontalGroup(
            jPanelTop3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnTop3, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
        );
        jPanelTop3Layout.setVerticalGroup(
            jPanelTop3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelTop3Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnTop3, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout jPanelPrincipalLayout = new javax.swing.GroupLayout(jPanelPrincipal);
        jPanelPrincipal.setLayout(jPanelPrincipalLayout);
        jPanelPrincipalLayout.setHorizontalGroup(
            jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPrincipalLayout.createSequentialGroup()
                .addGap(83, 83, 83)
                .addComponent(jPanelInfoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE)
                .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                        .addComponent(jpBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(38, 38, 38)
                        .addComponent(jPanelTop3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(642, 642, 642)
                        .addComponent(btnsalir))
                    .addComponent(jPanelProyectosGeneral, javax.swing.GroupLayout.PREFERRED_SIZE, 988, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21))
        );
        jPanelPrincipalLayout.setVerticalGroup(
            jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                        .addGap(78, 78, 78)
                        .addComponent(jPanelInfoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 457, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                        .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanelTop3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jPanelProyectosGeneral, javax.swing.GroupLayout.PREFERRED_SIZE, 572, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jpBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(104, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        int respuesta = JOptionPane.showConfirmDialog(null, "¿Estás seguro?");
        if(respuesta == 0){
            Main.guardarDatos();
            System.exit(0);
        }
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btncerrarsesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncerrarsesionActionPerformed
        this.setVisible(false);
        Main.setUsuarioLogeado("");
        Main.login.limpiarInfo();
        Main.login.setVisible(true);
    }//GEN-LAST:event_btncerrarsesionActionPerformed

    private void btneditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btneditarActionPerformed
        setVisible(false);
        new Modificar_cuenta_E().setVisible(true);
    }//GEN-LAST:event_btneditarActionPerformed

    private void btnagregarproyectoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarproyectoActionPerformed
        this.setVisible(false);
        String[] info = {};
        new CrearProyecto(" ", info, false).setVisible(true);
    }//GEN-LAST:event_btnagregarproyectoActionPerformed

    private void jlSearchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlSearchMouseClicked
        // TODO add your handling code here:
        String busqueda = jtxtSearch.getText();
        Emprendedor emp = Main.emprendedores.get(Main.getUsuarioLogeado());
        DynamicArray<Proyecto> resultado = emp.buscarProyecto(busqueda, emp.verProyectosArray());

        if (resultado.isEmpty()) {
            JOptionPane.showMessageDialog(null, "No se encontró ningún proyecto", "", JOptionPane.INFORMATION_MESSAGE);
        } else {
            this.arreglo = resultado;
            actualizarEscroll();
        }
    }//GEN-LAST:event_jlSearchMouseClicked

    private void jlSearchMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlSearchMouseEntered
        // TODO add your handling code here:
        jlSearch.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }//GEN-LAST:event_jlSearchMouseEntered

    private void jtxtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtxtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtxtSearchActionPerformed

    private void btnsalirMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnsalirMouseEntered
        // TODO add your handling code here:
        btnsalir.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }//GEN-LAST:event_btnsalirMouseEntered

    private void btneditarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneditarMouseEntered
        // TODO add your handling code here:
        btneditar.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }//GEN-LAST:event_btneditarMouseEntered

    private void btncerrarsesionMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncerrarsesionMouseEntered
        // TODO add your handling code here:
        btncerrarsesion.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }//GEN-LAST:event_btncerrarsesionMouseEntered

    private void btnagregarproyectoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnagregarproyectoMouseEntered
        // TODO add your handling code here:
        btnagregarproyecto.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }//GEN-LAST:event_btnagregarproyectoMouseEntered

    private void btnTop3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnTop3MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnTop3MouseEntered

    private void btnTop3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTop3ActionPerformed
        this.setVisible(false);
        Estadisticas estadisticaIndividual = new Estadisticas();
        estadisticaIndividual.setVisible(true);
    }//GEN-LAST:event_btnTop3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MenuEmprendedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MenuEmprendedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MenuEmprendedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MenuEmprendedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                String usuario = String.valueOf(LogIn.usuario);
                new MenuEmprendedor().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnTop3;
    private javax.swing.JButton btnagregarproyecto;
    private javax.swing.JButton btncerrarsesion;
    private javax.swing.JButton btneditar;
    private javax.swing.JButton btnsalir;
    private javax.swing.JLabel jLabelCiudadUsuario;
    private javax.swing.JLabel jLabelCorreoUsuario;
    private javax.swing.JLabel jLabelFotoUsuario;
    private javax.swing.JLabel jLabelNombreUsuario;
    private javax.swing.JLabel jLabelUsuario;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanelAgregarProyecto;
    private javax.swing.JPanel jPanelEditar;
    private javax.swing.JPanel jPanelInfoUsuario;
    private javax.swing.JPanel jPanelPrincipal;
    private javax.swing.JPanel jPanelProyectosGeneral;
    private javax.swing.JPanel jPanelTop3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JLabel jlSearch;
    private javax.swing.JPanel jpBuscar;
    private javax.swing.JTextField jtxtSearch;
    // End of variables declaration//GEN-END:variables
}
