/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import businessLogic.DynamicArray;
import businessLogic.LinkedList;
import businessLogic.Main;
import businessLogic.MaxHeap;
import businessLogic.Nodo;
import data.Proyecto;
import static gui.LogIn.usuario;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 *
 * @author angel
 */
public class Estadisticas extends javax.swing.JFrame {
    private boolean istopgeneral ;
    private boolean istopemprendedor;
    private String categoria = null;
    int ID1,ID2,ID3;

    public boolean isIstopgeneral() {
        return istopgeneral;
    }

    public void setIstopgeneral(boolean istopgeneral) {
        this.istopgeneral = istopgeneral;
    }

    public boolean isIstopemprendedor() {
        return istopemprendedor;
    }

    public void setIstopemprendedor(boolean istopemprendedor) {
        this.istopemprendedor = istopemprendedor;
    }

    

    public Estadisticas() {

        istopgeneral = Main.istopgeneral;
        istopemprendedor = Main.getEmprendedores().containsKey(Main.getUsuarioLogeado());
        categoria = Main.categoria;
        System.out.println(categoria);
        initComponents();
        setResizable(false);
//        Dimension dimension = new Dimension(800, 750);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
//        setSize(dimension);
//        setMaximumSize(dimension);
//        setMinimumSize(dimension);
        this.setLocation(
                screenSize.width / 2 - this.getWidth() / 2,
                screenSize.height / 2 - this.getHeight() / 2
        );
        //Main.getEmprendedores().containsKey(Main.getUsuarioLogeado())
        jLabelNombreTop.setHorizontalAlignment(SwingConstants.CENTER);
        jLabelNombreTop.setVerticalAlignment(SwingConstants.CENTER);
        if (istopgeneral) {
            obtenerEstadisticas(Main.getInversionistas().get(Main.getUsuarioLogeado()).generarTop3General());
        } else {
            if (istopemprendedor) {
                btnvertopgeneral.setVisible(false);
                obtenerEstadisticas(Main.getEmprendedores().get(Main.getUsuarioLogeado()).generarTop3());
            } else {
                obtenerEstadisticas(Main.getInversionistas().get(Main.getUsuarioLogeado()).generarTop3Categoria(categoria));
            }
        }

    } 
    
    public void obtenerEstadisticas(MaxHeap Top3Proyectos){
        int size = Top3Proyectos.getSize();
        System.out.println("el tamaño es: " + size);
        JPanel[] paneles = {jPanelProy1,jPanelProy2,jPanelProy3};
        if(size == 0){
            for (int i = 0; i < 3; i++) {
                paneles[i].setVisible(false);
            }
            if(!(istopemprendedor)){
                jLabelNombreProyecto1.setText("No hay proyectos para mostrar");
                if(!(istopgeneral)){
                   jLabelNombreTop.setText("Top de la categoria " + categoria);
                }else{
                   jLabelNombreTop.setText("Top general"); 
                }
            }else{
               jLabelNombreTop.setText("Top de tus proyectos"); 
               jLabelNombreProyecto1.setText("No tienes proyectos para mostrar");
            }
            jLabelNombreProyecto1.setForeground(Color.WHITE);
            jLabelNombreProyecto1.setFont(new Font(jLabelNombreProyecto1.getFont().getName(), Font.BOLD, 24));
            jLabelCategoria1.setVisible(false);
            jTextAreaDescripcion1.setVisible(false);
            btneditar1.setVisible(false);
            btneliminar1.setVisible(false);
            jLabelIconoProy1.setVisible(false);
            jScrollPane1.setVisible(false);
            jLabelNombreProyecto1.setLocation(jPanelProy1.getWidth() / 2 - jLabelNombreProyecto1.getWidth() / 2,
                jPanelProy1.getHeight() / 2 - jLabelNombreProyecto1.getHeight() / 2);
            jPanelProy1.setBackground(new Color(252,201,61));
            jPanelProy1.setVisible(true);
            return;
        }
        int i = 0;
        while(Top3Proyectos.getSize() > 0 && i < 3){
            Proyecto proyecto = Top3Proyectos.extractMax();
            if(i == 0){
                ID1 = proyecto.getId();
                jLabelNombreProyecto1.setText(proyecto.getNombre());
                jLabelCategoria1.setText(proyecto.getCategoria());
                jTextAreaDescripcion1.setText(proyecto.getDescripcion());
                
                jLabelNombreProyecto1.setForeground(Color.BLACK);
                jLabelNombreProyecto1.setFont(new Font(jLabelNombreProyecto2.getFont().getName(), Font.BOLD, 12));
                jLabelCategoria1.setVisible(true);
                jTextAreaDescripcion1.setVisible(true);
                btneditar1.setVisible(true);
                btneliminar1.setVisible(true);
                jLabelIconoProy1.setVisible(true);
                jScrollPane1.setVisible(true);
                jPanelProy1.setBackground(Color.WHITE);
                jPanelProy1.setVisible(true);
            }
            if(i == 1){
                ID2 = proyecto.getId();
                jLabelNombreProyecto2.setText(proyecto.getNombre());
                jLabelCategoria2.setText(proyecto.getCategoria());
                jTextAreaDescripcion2.setText(proyecto.getDescripcion());
                jPanelProy2.setVisible(true);
            }
            if(i == 2){
                ID3 = proyecto.getId();
                jLabelNombreProyecto3.setText(proyecto.getNombre());
                jLabelCategoria3.setText(proyecto.getCategoria());
                jTextAreaDescripcion3.setText(proyecto.getDescripcion());
                jPanelProy3.setVisible(true);
            }
            i++;
        }
        if(size < 3 ){
            for (int j = 0; j < (3 - size ); j++) {
                paneles[paneles.length -j-1].setVisible(false);
            }
        }

        if (istopgeneral) {
            jLabelNombreTop.setText("Top general");
            if (!(istopemprendedor)) {
                btneditar1.setVisible(false);
                btneliminar1.setVisible(false);
                btneditar2.setVisible(false);
                btneliminar2.setVisible(false);
                btneditar3.setVisible(false);
                btneliminar3.setVisible(false);
                btnvertopgeneral.setText("TOP POR CATEGORIAS");
            }
        } else {
            if (istopemprendedor) {
                jLabelNombreTop.setText("Top de tus proyectos");
            } else {
                btneditar1.setVisible(false);
                btneliminar1.setVisible(false);
                btneditar2.setVisible(false);
                btneliminar2.setVisible(false);
                btneditar3.setVisible(false);
                btneliminar3.setVisible(false);
                jLabelNombreTop.setText("Top de la categoria " + categoria);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labeltitulo = new javax.swing.JLabel();
        jPanelPrincipal = new javax.swing.JPanel();
        btnsalir = new javax.swing.JButton();
        btnvolver = new javax.swing.JButton();
        jLabelNombreTop = new javax.swing.JLabel();
        jPanelProy1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaDescripcion1 = new javax.swing.JTextArea();
        jLabelNombreProyecto1 = new javax.swing.JLabel();
        jLabelCategoria1 = new javax.swing.JLabel();
        btneditar1 = new javax.swing.JButton();
        btneliminar1 = new javax.swing.JButton();
        jLabelIconoProy1 = new javax.swing.JLabel();
        jPanelVerTopGeneral = new javax.swing.JPanel();
        btnvertopgeneral = new javax.swing.JButton();
        jPanelProy2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaDescripcion2 = new javax.swing.JTextArea();
        jLabelNombreProyecto2 = new javax.swing.JLabel();
        jLabelCategoria2 = new javax.swing.JLabel();
        btneditar2 = new javax.swing.JButton();
        btneliminar2 = new javax.swing.JButton();
        jLabelIconoProy2 = new javax.swing.JLabel();
        jPanelProy3 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextAreaDescripcion3 = new javax.swing.JTextArea();
        jLabelNombreProyecto3 = new javax.swing.JLabel();
        jLabelCategoria3 = new javax.swing.JLabel();
        btneditar3 = new javax.swing.JButton();
        btneliminar3 = new javax.swing.JButton();
        jLabelIconoProy3 = new javax.swing.JLabel();

        labeltitulo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/close.png"))); // NOI18N
        labeltitulo.setText("jLabel1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanelPrincipal.setBackground(new java.awt.Color(252, 201, 61));
        jPanelPrincipal.setName(""); // NOI18N

        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/close.png"))); // NOI18N
        btnsalir.setBorderPainted(false);
        btnsalir.setContentAreaFilled(false);
        btnsalir.setFocusPainted(false);
        btnsalir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnsalirMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnsalirMouseEntered(evt);
            }
        });
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        btnvolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/arrow.png"))); // NOI18N
        btnvolver.setBorderPainted(false);
        btnvolver.setContentAreaFilled(false);
        btnvolver.setFocusPainted(false);
        btnvolver.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnvolverMouseEntered(evt);
            }
        });
        btnvolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnvolverActionPerformed(evt);
            }
        });

        jLabelNombreTop.setFont(new java.awt.Font("Tahoma", 3, 60)); // NOI18N
        jLabelNombreTop.setForeground(new java.awt.Color(255, 255, 255));
        jLabelNombreTop.setText("TOP");

        jPanelProy1.setBackground(new java.awt.Color(255, 255, 255));
        jPanelProy1.setPreferredSize(new java.awt.Dimension(968, 120));

        jTextAreaDescripcion1.setColumns(20);
        jTextAreaDescripcion1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTextAreaDescripcion1.setRows(5);
        jScrollPane1.setViewportView(jTextAreaDescripcion1);

        jLabelNombreProyecto1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelNombreProyecto1.setText("Nombre proyecto");

        jLabelCategoria1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelCategoria1.setForeground(new java.awt.Color(252, 201, 61));
        jLabelCategoria1.setText("Categoria:");

        btneditar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/edit.png"))); // NOI18N
        btneditar1.setBorderPainted(false);
        btneditar1.setContentAreaFilled(false);
        btneditar1.setFocusPainted(false);
        btneditar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btneditar1ActionPerformed(evt);
            }
        });

        btneliminar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/delete.png"))); // NOI18N
        btneliminar1.setBorderPainted(false);
        btneliminar1.setContentAreaFilled(false);
        btneliminar1.setFocusPainted(false);
        btneliminar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btneliminar1ActionPerformed(evt);
            }
        });

        jLabelIconoProy1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/first.png"))); // NOI18N

        javax.swing.GroupLayout jPanelProy1Layout = new javax.swing.GroupLayout(jPanelProy1);
        jPanelProy1.setLayout(jPanelProy1Layout);
        jPanelProy1Layout.setHorizontalGroup(
            jPanelProy1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelProy1Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabelIconoProy1)
                .addGap(18, 18, 18)
                .addGroup(jPanelProy1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanelProy1Layout.createSequentialGroup()
                        .addComponent(jLabelNombreProyecto1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelCategoria1))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 719, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addComponent(btneditar1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(btneliminar1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30))
        );
        jPanelProy1Layout.setVerticalGroup(
            jPanelProy1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelProy1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelProy1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNombreProyecto1)
                    .addComponent(jLabelCategoria1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelProy1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelProy1Layout.createSequentialGroup()
                        .addGroup(jPanelProy1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btneliminar1)
                            .addComponent(btneditar1))
                        .addGap(44, 44, 44))
                    .addGroup(jPanelProy1Layout.createSequentialGroup()
                        .addGroup(jPanelProy1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelIconoProy1)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(16, 16, 16))))
        );

        jPanelVerTopGeneral.setBackground(new java.awt.Color(251, 133, 46));

        btnvertopgeneral.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        btnvertopgeneral.setForeground(new java.awt.Color(255, 255, 255));
        btnvertopgeneral.setText("VER TOP GENERAL");
        btnvertopgeneral.setBorderPainted(false);
        btnvertopgeneral.setContentAreaFilled(false);
        btnvertopgeneral.setFocusPainted(false);
        btnvertopgeneral.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnvertopgeneralActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelVerTopGeneralLayout = new javax.swing.GroupLayout(jPanelVerTopGeneral);
        jPanelVerTopGeneral.setLayout(jPanelVerTopGeneralLayout);
        jPanelVerTopGeneralLayout.setHorizontalGroup(
            jPanelVerTopGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnvertopgeneral, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
        );
        jPanelVerTopGeneralLayout.setVerticalGroup(
            jPanelVerTopGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelVerTopGeneralLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnvertopgeneral, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanelProy2.setBackground(new java.awt.Color(255, 255, 255));
        jPanelProy2.setPreferredSize(new java.awt.Dimension(968, 120));

        jTextAreaDescripcion2.setColumns(20);
        jTextAreaDescripcion2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTextAreaDescripcion2.setRows(5);
        jScrollPane2.setViewportView(jTextAreaDescripcion2);

        jLabelNombreProyecto2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelNombreProyecto2.setText("Nombre proyecto");

        jLabelCategoria2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelCategoria2.setForeground(new java.awt.Color(252, 201, 61));
        jLabelCategoria2.setText("Categoria:");

        btneditar2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/edit.png"))); // NOI18N
        btneditar2.setBorderPainted(false);
        btneditar2.setContentAreaFilled(false);
        btneditar2.setFocusPainted(false);
        btneditar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btneditar2ActionPerformed(evt);
            }
        });

        btneliminar2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/delete.png"))); // NOI18N
        btneliminar2.setBorderPainted(false);
        btneliminar2.setContentAreaFilled(false);
        btneliminar2.setFocusPainted(false);
        btneliminar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btneliminar2ActionPerformed(evt);
            }
        });

        jLabelIconoProy2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/second-prize.png"))); // NOI18N

        javax.swing.GroupLayout jPanelProy2Layout = new javax.swing.GroupLayout(jPanelProy2);
        jPanelProy2.setLayout(jPanelProy2Layout);
        jPanelProy2Layout.setHorizontalGroup(
            jPanelProy2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelProy2Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabelIconoProy2)
                .addGap(18, 18, 18)
                .addGroup(jPanelProy2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanelProy2Layout.createSequentialGroup()
                        .addComponent(jLabelNombreProyecto2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelCategoria2))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 719, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addComponent(btneditar2, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(btneliminar2, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30))
        );
        jPanelProy2Layout.setVerticalGroup(
            jPanelProy2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelProy2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelProy2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNombreProyecto2)
                    .addComponent(jLabelCategoria2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelProy2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelProy2Layout.createSequentialGroup()
                        .addGroup(jPanelProy2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btneliminar2)
                            .addComponent(btneditar2))
                        .addGap(44, 44, 44))
                    .addGroup(jPanelProy2Layout.createSequentialGroup()
                        .addGroup(jPanelProy2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelIconoProy2)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(16, 16, 16))))
        );

        jPanelProy3.setBackground(new java.awt.Color(255, 255, 255));
        jPanelProy3.setPreferredSize(new java.awt.Dimension(968, 120));

        jTextAreaDescripcion3.setColumns(20);
        jTextAreaDescripcion3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTextAreaDescripcion3.setRows(5);
        jScrollPane3.setViewportView(jTextAreaDescripcion3);

        jLabelNombreProyecto3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelNombreProyecto3.setText("Nombre proyecto");

        jLabelCategoria3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelCategoria3.setForeground(new java.awt.Color(252, 201, 61));
        jLabelCategoria3.setText("Categoria:");

        btneditar3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/edit.png"))); // NOI18N
        btneditar3.setBorderPainted(false);
        btneditar3.setContentAreaFilled(false);
        btneditar3.setFocusPainted(false);
        btneditar3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btneditar3ActionPerformed(evt);
            }
        });

        btneliminar3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/delete.png"))); // NOI18N
        btneliminar3.setBorderPainted(false);
        btneliminar3.setContentAreaFilled(false);
        btneliminar3.setFocusPainted(false);
        btneliminar3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btneliminar3ActionPerformed(evt);
            }
        });

        jLabelIconoProy3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/third-prize.png"))); // NOI18N

        javax.swing.GroupLayout jPanelProy3Layout = new javax.swing.GroupLayout(jPanelProy3);
        jPanelProy3.setLayout(jPanelProy3Layout);
        jPanelProy3Layout.setHorizontalGroup(
            jPanelProy3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelProy3Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabelIconoProy3)
                .addGap(18, 18, 18)
                .addGroup(jPanelProy3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanelProy3Layout.createSequentialGroup()
                        .addComponent(jLabelNombreProyecto3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelCategoria3))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 719, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addComponent(btneditar3, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(btneliminar3, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30))
        );
        jPanelProy3Layout.setVerticalGroup(
            jPanelProy3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelProy3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelProy3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNombreProyecto3)
                    .addComponent(jLabelCategoria3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelProy3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelProy3Layout.createSequentialGroup()
                        .addGroup(jPanelProy3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btneliminar3)
                            .addComponent(btneditar3))
                        .addGap(44, 44, 44))
                    .addGroup(jPanelProy3Layout.createSequentialGroup()
                        .addGroup(jPanelProy3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelIconoProy3)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(16, 16, 16))))
        );

        javax.swing.GroupLayout jPanelPrincipalLayout = new javax.swing.GroupLayout(jPanelPrincipal);
        jPanelPrincipal.setLayout(jPanelPrincipalLayout);
        jPanelPrincipalLayout.setHorizontalGroup(
            jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                        .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                                .addComponent(btnvolver)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jPanelProy2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jPanelProy1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jPanelProy3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 35, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPrincipalLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanelVerTopGeneral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(403, 403, 403))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPrincipalLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabelNombreTop, javax.swing.GroupLayout.PREFERRED_SIZE, 816, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(99, 99, 99))
        );
        jPanelPrincipalLayout.setVerticalGroup(
            jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnvolver))
                .addGap(8, 8, 8)
                .addComponent(jLabelNombreTop)
                .addGap(43, 43, 43)
                .addComponent(jPanelProy1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47)
                .addComponent(jPanelProy2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 44, Short.MAX_VALUE)
                .addComponent(jPanelProy3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(jPanelVerTopGeneral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(54, 54, 54))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnvolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnvolverActionPerformed
        this.setVisible(false);
        if(istopemprendedor){
            DynamicArray<Proyecto> lista = Main.getEmprendedores().get(Main.getUsuarioLogeado()).verProyectosArray();
            Main.login.menuEmprendedor = new MenuEmprendedor(lista);
            Main.login.menuEmprendedor.setVisible(true);
        }else{
            Main.getLogin().getMenuInversionista().setVisible(true);
        }
    }//GEN-LAST:event_btnvolverActionPerformed

    private void btnsalirMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnsalirMouseEntered
        // TODO add your handling code here:
        btnsalir.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }//GEN-LAST:event_btnsalirMouseEntered

    private void btnvolverMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnvolverMouseEntered
        // TODO add your handling code here:
        btnvolver.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }//GEN-LAST:event_btnvolverMouseEntered

    private void btneditar3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btneditar3ActionPerformed
        this.setVisible(false);
        new ModificarProyecto(Main.getProyectos().get(ID3), categoria, false).setVisible(true);
    }//GEN-LAST:event_btneditar3ActionPerformed

    private void btnvertopgeneralActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnvertopgeneralActionPerformed
        if(istopemprendedor){
            if(istopgeneral){
                istopgeneral = false;
                obtenerEstadisticas(Main.getEmprendedores().get(Main.getUsuarioLogeado()).generarTop3());
            }
        }else{
            if(istopgeneral){
                Main.istopgeneral = false;
                istopgeneral = false;
                new Menu_Top_Categoria().setVisible(true);
                this.setVisible(false);
            }else{
                istopgeneral = true;
                obtenerEstadisticas(Main.getInversionistas().get(Main.getUsuarioLogeado()).generarTop3General());
            }
        }
    }//GEN-LAST:event_btnvertopgeneralActionPerformed

    private void btneditar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btneditar1ActionPerformed
        this.setVisible(false);
        new ModificarProyecto(Main.getProyectos().get(ID1), categoria, false).setVisible(true);
    }//GEN-LAST:event_btneditar1ActionPerformed

    private void btneditar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btneditar2ActionPerformed
        this.setVisible(false);
        new ModificarProyecto(Main.getProyectos().get(ID3), categoria, false).setVisible(true);
    }//GEN-LAST:event_btneditar2ActionPerformed

    private void btneliminar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btneliminar1ActionPerformed
        int respuesta = JOptionPane.showConfirmDialog(null, "¿Deseas eleminar este proyecto?");
        if (respuesta == 0) {
            Main.getEmprendedores().get(Main.getUsuarioLogeado()).eliminarProyecto(ID1);
            obtenerEstadisticas(Main.getEmprendedores().get(Main.getUsuarioLogeado()).generarTop3());
        }
    }//GEN-LAST:event_btneliminar1ActionPerformed

    private void btneliminar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btneliminar2ActionPerformed
        int respuesta = JOptionPane.showConfirmDialog(null, "¿Deseas eleminar este proyecto?");
        if (respuesta == 0) {
            Main.getEmprendedores().get(Main.getUsuarioLogeado()).eliminarProyecto(ID2);
            obtenerEstadisticas(Main.getEmprendedores().get(Main.getUsuarioLogeado()).generarTop3());
        }
    }//GEN-LAST:event_btneliminar2ActionPerformed

    private void btneliminar3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btneliminar3ActionPerformed
        int respuesta = JOptionPane.showConfirmDialog(null, "¿Deseas eleminar este proyecto?");
        if (respuesta == 0) {
            Main.getEmprendedores().get(Main.getUsuarioLogeado()).eliminarProyecto(ID3);
            obtenerEstadisticas(Main.getEmprendedores().get(Main.getUsuarioLogeado()).generarTop3());
        }
    }//GEN-LAST:event_btneliminar3ActionPerformed

    private void btnsalirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnsalirMouseClicked
        // TODO add your handling code here:
        int respuesta = JOptionPane.showConfirmDialog(null, "¿Estás seguro?");
        if(respuesta == 0){
            Main.guardarDatos();
            System.exit(0);
        }
    }//GEN-LAST:event_btnsalirMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Estadisticas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Estadisticas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Estadisticas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Estadisticas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run(){ 
                new Estadisticas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btneditar1;
    private javax.swing.JButton btneditar2;
    private javax.swing.JButton btneditar3;
    private javax.swing.JButton btneliminar1;
    private javax.swing.JButton btneliminar2;
    private javax.swing.JButton btneliminar3;
    private javax.swing.JButton btnsalir;
    private javax.swing.JButton btnvertopgeneral;
    private javax.swing.JButton btnvolver;
    private javax.swing.JLabel jLabelCategoria1;
    private javax.swing.JLabel jLabelCategoria2;
    private javax.swing.JLabel jLabelCategoria3;
    private javax.swing.JLabel jLabelIconoProy1;
    private javax.swing.JLabel jLabelIconoProy2;
    private javax.swing.JLabel jLabelIconoProy3;
    private javax.swing.JLabel jLabelNombreProyecto1;
    private javax.swing.JLabel jLabelNombreProyecto2;
    private javax.swing.JLabel jLabelNombreProyecto3;
    private javax.swing.JLabel jLabelNombreTop;
    private javax.swing.JPanel jPanelPrincipal;
    private javax.swing.JPanel jPanelProy1;
    private javax.swing.JPanel jPanelProy2;
    private javax.swing.JPanel jPanelProy3;
    private javax.swing.JPanel jPanelVerTopGeneral;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextArea jTextAreaDescripcion1;
    private javax.swing.JTextArea jTextAreaDescripcion2;
    private javax.swing.JTextArea jTextAreaDescripcion3;
    private javax.swing.JLabel labeltitulo;
    // End of variables declaration//GEN-END:variables
}
