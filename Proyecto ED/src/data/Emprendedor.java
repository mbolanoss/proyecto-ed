package data;

import businessLogic.DynamicArray;
import businessLogic.LinkedList;
import businessLogic.Main;
import businessLogic.MaxHeap;
import businessLogic.Nodo;
import businessLogic.Validacion;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Scanner;


public class Emprendedor extends Usuario{
    
    public Emprendedor(String nombre, String usuario, String contrasena,
            String ciudad, int cedula, String email) {
        super(nombre, contrasena, usuario, ciudad, cedula, true, email);
    }
    
    public MaxHeap generarTop3(){
        MaxHeap proyectosOrdenados = new MaxHeap();
        for (Proyecto p : Main.getProyectos().values(Proyecto.class)) {
            if(p.getIdCreador().equals(this.getUsuario())){
                proyectosOrdenados.insert(p);
            }
        }
        proyectosOrdenados.maxHeap();
//        while (listaProyectos.size() < 3) { 
//            if(proyectosOrdenados.getSize() == 0){
//                break;
//            }
//            listaProyectos.pushBack(proyectosOrdenados.extractMax());
//        }
//        return listaProyectos;
        return proyectosOrdenados;
    }
    
    public void crearProyecto(int id, String nombre, String descripcion, String categoria, 
            long presupuesto, String idCreador, String alcance){
        Main.getProyectos().put(new Proyecto(id, nombre, descripcion, categoria, 
             presupuesto, idCreador, alcance), id);
    }
    
    public void eliminarProyecto(int id){
        Main.getProyectos().remove(id);
    }
    
    public LinkedList<Proyecto> verProyectosLinked(){
        LinkedList<Proyecto> listaProyectos = new LinkedList<>();
        for(Proyecto p : Main.getProyectos().values(Proyecto.class)){
            if(p.getIdCreador().equals(this.getUsuario())){
                listaProyectos.pushFront(p);
            }
        }
        return listaProyectos;
    }
    
    public DynamicArray<Proyecto> verProyectosArray(){
        DynamicArray<Proyecto> arrProyectos = new DynamicArray<>(5,Proyecto.class);
        for(Proyecto p : Main.getProyectos().values(Proyecto.class)){
            if(p.getIdCreador().equals(this.getUsuario())){
                //System.out.println(p.toString());
                arrProyectos.insert(p);
            }
        }
        return arrProyectos;
    }
    
}
