package data;

import businessLogic.DynamicArray;
import businessLogic.LinkedList;
import businessLogic.Main;
import businessLogic.MaxHeap;
import businessLogic.Nodo;
import businessLogic.Validacion;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Inversionista extends Usuario {

    private String[] preferencias;
    private LinkedList<Integer> interesados;

    public Inversionista(String[] preferencias, String nombre, String contrasena, String usuario, String ciudad, int cedula, String email) {
        super(nombre, contrasena, usuario, ciudad, cedula, false, email);
        this.preferencias = preferencias;
        this.interesados = new LinkedList<>();
    }

    /*public LinkedList<Proyecto> filtrarProyectosLinked() {//Se usa para filtrar los proyectos que le pueden interesar al inversionista
        LinkedList<Proyecto> listaProyectos = new LinkedList<>();
        MaxHeap proyectosOrdenados = new MaxHeap();
        for (Proyecto p : Main.getProyectos().values()) {
            if(!(this.interesados.contains(p.getId()))){
                for (String categoria : this.preferencias) {
                    if (categoria.equals(p.getCategoria())) {
                        proyectosOrdenados.insert(p);
                    }
                }
            }
        }
        //proyectosOrdenados.maxHeap();
        while (proyectosOrdenados.getSize() > 0) { 
            Proyecto proye = proyectosOrdenados.extractMax();
            listaProyectos.pushBack(proye);
        }
        return listaProyectos;
    }**/
    
    public LinkedList<Proyecto> filtrarProyectosLinked() {//Se usa para filtrar los proyectos que le pueden interesar al inversionista
        LinkedList<Proyecto> listaProyectos = new LinkedList<>();
        for (Proyecto p : Main.getProyectos().values(Proyecto.class)) {
            if(!(this.interesados.contains(p.getId()))){
                for (String categoria : this.preferencias) {
                    if (categoria.equals(p.getCategoria())) {
                        //System.out.println(p.toString());
                        listaProyectos.pushFront(p);
                    }
                }
            }
        }
        return listaProyectos;
    }
    
    public DynamicArray<Proyecto> filtrarProyectosArray(){
        DynamicArray<Proyecto> arrProyectos = new DynamicArray<>(5,Proyecto.class);
        for (Proyecto p : Main.getProyectos().values(Proyecto.class)) {
            if(!(this.interesados.contains(p.getId()))){
                for (String categoria : this.preferencias) {
                    if (categoria.equals(p.getCategoria())) {
                        arrProyectos.insert(p);
                    }
                }
            }
        }
        return arrProyectos;
    }
    
    public MaxHeap generarTop3General(){
        MaxHeap proyectosOrdenados = new MaxHeap();
        for (Proyecto p : Main.getProyectos().values(Proyecto.class)) {
            //System.out.println(p.getLikes());
            proyectosOrdenados.insert(p);
        }
        proyectosOrdenados.maxHeap();
//        int cont = 1;
//        while(proyectosOrdenados.getSize() > 0){
////            if(cont > 3){
////                break;
////            }
//            Proyecto p = proyectosOrdenados.extractMax();
//            System.out.println(p.getNombre() +" con el id " + p.getId() + " tiene " + p.getLikes() + " likes");
//            cont++;
//        }
//        System.out.println(" ");
        return proyectosOrdenados;
    }
    
    public MaxHeap generarTop3Categoria(String categoria){
        MaxHeap proyectosOrdenados = new MaxHeap();
        for (Proyecto p : Main.getProyectos().values(Proyecto.class)) {
            if(categoria.equals(p.getCategoria())){
                proyectosOrdenados.insert(p);
            }
        }
        proyectosOrdenados.maxHeap();
        return proyectosOrdenados;
    }

    public void eliminarProyecto(int id){
        this.interesados.remove(id);
    }
    
    public String[] getPreferencias() {
        return preferencias;
    }

    public void setPreferencias(String[] preferencias) {
        this.preferencias = preferencias;
    }

    public LinkedList<Integer> getInteresados() {
        return interesados;
    }

    public void setInteresados(LinkedList<Integer> interesados) {
        this.interesados = interesados;
    }

    public LinkedList<Proyecto> verProyectosInteresados() {
        LinkedList<Proyecto> proyectos = new LinkedList<>();
        Nodo<Integer> temp = this.interesados.getHead().getNext();
        int tempSize = this.interesados.getSize();
        while (tempSize != 0) {
            Proyecto p = Main.getProyectos().get(temp.getKey());
            proyectos.pushFront(p);
            temp = temp.getNext();
            tempSize--;
        }
        return proyectos;
    }

}
