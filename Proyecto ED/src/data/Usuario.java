package data;

import businessLogic.DynamicArray;
import businessLogic.LinkedList;
import java.io.Serializable;
import businessLogic.Main;
import static businessLogic.Main.emprendedores;
import businessLogic.MaxHeap;
import businessLogic.Nodo;
import businessLogic.Validacion;
import java.util.HashMap;
import java.util.Scanner;


public class Usuario implements Serializable{
    private String nombre;
    private String usuario;
    private String contrasena;
    private String ciudad;
    private int cedula;
    private boolean esEmprendedor;
    private String email;

    public Usuario(String nombre, String contrasena, String usuario, String ciudad, int cedula, 
            boolean esEmprendedor, String email) {
        this.nombre = nombre;
        this.usuario = usuario;
        this.ciudad = ciudad;
        this.cedula = cedula;
        this.esEmprendedor = esEmprendedor;
        this.email = email;
        this.contrasena = contrasena;
    }
    
    public LinkedList<Proyecto> buscarProyecto(String busqueda, LinkedList<Proyecto> fuente){
        busqueda = busqueda.toLowerCase();
        
        LinkedList<Proyecto> resultado = new LinkedList<>();
        LinkedList<Proyecto> listaProyectos = fuente;
        int tempSize = listaProyectos.getSize();
        Nodo<Proyecto> temp = listaProyectos.getHead().getNext();
        
        String nombre = "";
        while(tempSize!=0){
            nombre = temp.getKey().getNombre();
            nombre = nombre.toLowerCase();
            if(nombre.contains(busqueda)){
                resultado.pushFront(temp.getKey());
            }
            temp = temp.getNext();
            tempSize--;
        }
        
        
        return resultado;
    }
    
    public DynamicArray<Proyecto> buscarProyecto(String busqueda, DynamicArray<Proyecto> fuente){
        busqueda = busqueda.toLowerCase();
        
        DynamicArray<Proyecto> resultado = new DynamicArray<>(5, Proyecto.class);
        String nombreProyecto = "";
        for(int i = 0; i < fuente.getSize(); i++){
            nombreProyecto = fuente.get(i).getNombre();
            nombreProyecto = nombreProyecto.toLowerCase();
            if(nombreProyecto.contains(busqueda)){
                resultado.insert(fuente.get(i));
            }
        }
        
        
        return resultado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public boolean isEsEmprendedor() {
        return esEmprendedor;
    }

    public void setEsEmprendedor(boolean esEmprendedor) {
        this.esEmprendedor = esEmprendedor;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Usuario{" + "nombre=" + nombre + ", usuario=" + usuario + ", contrasena=" + contrasena + ", ciudad=" + ciudad + ", cedula=" + cedula + ", esEmprendedor=" + esEmprendedor + ", email=" + email + '}';
    }
} 
