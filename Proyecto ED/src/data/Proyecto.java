package data;

import java.io.Serializable;

public class Proyecto implements Serializable {

    private int id;
    private String nombre;
    private String descripcion;
    private String categoria;
    private long presupuesto;
    private String idCreador;
    private String alcance;
    private int likes;
    private int inversionTotal;
    //Imagenes y videos

    public Proyecto(int likes, int size) {
        this.id = size;
        this.nombre = " ";
        this.descripcion = " ";
        this.categoria = " ";
        this.presupuesto = 0;
        this.idCreador = " ";
        this.alcance = " ";
        this.likes = likes;
        this.inversionTotal = 0;
    }

    public Proyecto(int id, String nombre, String descripcion, String categoria, long presupuesto, String idCreador, String alcance) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.categoria = categoria;
        this.presupuesto = presupuesto;
        this.idCreador = idCreador;
        this.alcance = alcance;
        this.likes = 0;
        this.inversionTotal = 0;
    }

    public Proyecto(int id, String nombre, String descripcion, String categoria, long presupuesto, String idCreador, String alcance, int likes) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.categoria = categoria;
        this.presupuesto = presupuesto;
        this.idCreador = idCreador;
        this.alcance = alcance;
        this.likes = likes;
        this.inversionTotal = 0;
    }

    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof Proyecto)) {
            return false;
        }

        Proyecto p = (Proyecto) obj;

        // Compare the data members and return accordingly  
        return p.getId() == this.id;
    }

    @Override
    public String toString() {
        return "Id: " + this.id + "\nNombre: " + this.nombre + "\nDescripcion: "
                + this.descripcion + "\nCategoria: " + this.categoria + "\nPresupuesto= $"
                + this.presupuesto + "\nAlcance: " + this.alcance;
    }
    
    public void aumentarInversion(int cantidad){
        this.inversionTotal += cantidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public long getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(long presupuesto) {
        this.presupuesto = presupuesto;
    }

    public String getIdCreador() {
        return idCreador;
    }

    public void setIdCreador(String idCreador) {
        this.idCreador = idCreador;
    }

    public String getAlcance() {
        return alcance;
    }

    public void setAlcance(String alcance) {
        this.alcance = alcance;
    }

    public int getId() {
        return id;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getInversionTotal() {
        return inversionTotal;
    }

    public void setInversionTotal(int inversionTotal) {
        this.inversionTotal = inversionTotal;
    }

    
    
}
